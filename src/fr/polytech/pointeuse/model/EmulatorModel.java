package fr.polytech.pointeuse.model;

import java.util.ArrayList;

import fr.polytech.pointeuse.model.company.SimpleEmployee;

public class EmulatorModel {

	// ===== Attributes =====
	private ArrayList<SimpleEmployee> simpleEmployeesList;
	
	// ===== Constructors =====
	public EmulatorModel() {
		simpleEmployeesList = new ArrayList<SimpleEmployee>();
	}
	
	// ===== Getters =====
	public ArrayList<SimpleEmployee> getSimpleEmployeesList() {
		return simpleEmployeesList;
	}
	
	// ===== Other methods =====
	public void addSimpleEmployee(SimpleEmployee emp) throws Exception {
		
		for(SimpleEmployee simpEmp : simpleEmployeesList) {
			if (emp.getID() == simpEmp.getID()) {
				throw new Exception("L'employé est déjà dans la liste.");
			}
		}
		
		simpleEmployeesList.add(emp);
	}
}
