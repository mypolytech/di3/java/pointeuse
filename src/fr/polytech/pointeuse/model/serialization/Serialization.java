package fr.polytech.pointeuse.model.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.model.company.SimpleEmployee;

/**
 * Tool to save and import data for the main app. It saves objects in .dat
 * files. Note that this tool will be replaced with Hibernate to save data in a
 * database rather than in files.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

public class Serialization {

	// == Attributes ==
	private FileOutputStream fos;
	private ObjectOutputStream oos;
	private FileInputStream fis;
	private ObjectInputStream ois;
	private String path;
	// ================
	
	// == Constructors ==
	public Serialization(String path) {
		this.path = path;
	}
	// ==================

	// == Other methods ==
	/**
	 * Saves the current state of the company in a .dat file.
	 * @param company the company to save
	 * @throws IOException
	 */
	public void saveToFile(Company company) throws IOException {

		fos = new FileOutputStream(path);
		oos = new ObjectOutputStream(fos);

		oos.writeObject(company);
		oos.writeInt(Employee.getNewID());
		oos.close();
	}

	/**
	 * Loads a company from a .dat file.
	 * @return the company saved in the file.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Company loadCompanyFromFile() throws IOException, ClassNotFoundException {

		fis = new FileInputStream(path);
		ois = new ObjectInputStream(fis);

		Company comp = new Company((Company) ois.readObject());

		Employee.setNewID(ois.readInt());

		return comp;
	}

	/**
	 * Loads the list of employees from the saved company.
	 * @return the list of employees.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public ArrayList<SimpleEmployee> loadEmployeesFromFile() throws IOException, ClassNotFoundException {

		fis = new FileInputStream(path);
		ois = new ObjectInputStream(fis);

		ArrayList<SimpleEmployee> list = new ArrayList<SimpleEmployee>();
		Company comp = new Company((Company) ois.readObject());

		for (Service serv : comp.getServices()) {
			for (Employee emp : serv.getEmployees()) {
				list.add(new SimpleEmployee(emp));
			}
		}

		return list;
	}
	// ===================
}
