package fr.polytech.pointeuse.model.company;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Boss class represents a boss. It contains the boss' name and the list of
 * companies they own.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

@Entity
public class Boss implements Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	@Id
	private int id;
	private String firstName;
	private String lastName;
	@Transient
	private ArrayList<Company> companies;
	// ================
	
	// == Constructors ==
	/**
	 * Initializes a newly created Boss object so that it represents an empty
	 * object. Note that this constructor is not really useful and should not be
	 * used.
	 */
	public Boss () {
		this ("N/A", "N/A");
	}
	
	/**
	 * Initializes a newly created Boss object so that it represents a company boss.
	 * The created boss has a first name and a last name. Note that, when created,
	 * the boss does not own any company.
	 * 
	 * @param fname the boss' first name.
	 * @param lname the boss' last name.
	 */
	public Boss (String fname, String lname) {
		this.firstName = fname;
		this.lastName = lname;
		this.companies = new ArrayList<>();
	}
	// ==================

	// == Getters ==
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public List<Company> getCompanies() {
		return companies;
	}
	
	public int getId() {
		return id;
	}
	// =============
	
	// == Setters ==
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = new ArrayList<>(companies);
	}
	// =============
	
	// == Other methods ==
	/**
	 * Add a company to the companies list.
	 * 
	 * @param company the company to be added.
	 * @throws BossException the company already exists in the list.
	 */
	public void addCompany (Company company) throws BossException {
		
		for (Company comp : companies) {
			if (comp.getcName().compareToIgnoreCase(company.getcName()) == 0) {
				throw new BossException("L'entreprise est déjà présente dans la liste.");
			}
		}
		
		companies.add(company);
		company.setBoss(this);
	}
}
