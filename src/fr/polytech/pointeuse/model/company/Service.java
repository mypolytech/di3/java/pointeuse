package fr.polytech.pointeuse.model.company;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Service class represents a service. It contains the service's name, its
 * work force and if it is managed or not. It also contains the list of
 * employees working in the service, the manager in charge and the company
 * owning the service.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 *
 */

@Entity
public class Service implements Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(Service.class);

	@Id
	private int id;
	private String serviceName;
	private int workforce;
	private boolean isManaged = false;
	@Transient
	private Company company;
	@Transient
	private Manager manager;
	@Transient
	private ArrayList<Employee> employees;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created Service object so that it represents an empty
	 * object. Note that this constructor is not really useful and should not be
	 * used.
	 */
	public Service() {
		this("N/A", null);
	}

	/**
	 * Initializes a newly created Service object so that it represents a service.
	 * The created service has a name. Note that, when created, the service is not
	 * associated to any company and has no employee nor manager.
	 * 
	 * @param serviceName service's name.
	 */
	public Service(String serviceName) {
		this(serviceName, null);
	}

	/**
	 * Initializes a newly created Service object so that it represents a service.
	 * The created service has a name and a manager. Note that, when created, the
	 * service is not associated to any company and has no employee.
	 * 
	 * @param serviceName service's name.
	 * @param manager     service's manager.
	 */
	public Service(String serviceName, Manager manager) {
		this.employees = new ArrayList<>();
		this.workforce = 0;
		this.serviceName = serviceName;
		this.manager = manager;

		if (manager != null) {
			this.isManaged = true;
			this.manager.setService(this);
		}
	}
	// ==================

	// == Getters ==
	public String getServiceName() {
		return serviceName;
	}

	public Company getCompany() {
		return company;
	}

	public int getWorkforce() {
		return workforce;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public Manager getManager() {
		return manager;
	}

	public boolean isManaged() {
		return isManaged;
	}

	public int getId() {
		return id;
	}
	// =============

	// == Setters ==
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setWorkforce() {
		this.workforce = employees.size();
	}

	/**
	 * To set the manager we need to check few things : is he already in charge of
	 * another service, does he work in the service and is the service already
	 * managed.
	 * 
	 * @param manager the manager
	 * @throws ServiceException if one of the defined conditions is not respected.
	 */
	public void setManager(Manager manager) throws ServiceException {
		if (manager.isManaging()) {
			throw new ServiceException("The employee already is in the service.");
		} else if (manager.getService() != this && manager.getService() != null) {
			throw new ServiceException("The manager is not working in this service.");
		} else {
			this.addEmployee(manager);

			if (isManaged) {
				throw new ServiceException("The service already has a manager.");
			} else {
				this.manager = manager;
				this.isManaged = true;
				manager.setManagingTrue();
			}
		}
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setWorkforce(int workforce) {
		this.workforce = workforce;
	}

	public void setManaged(boolean managed) {
		isManaged = managed;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = new ArrayList<>(employees);
	}
	// =============

	// == Other methods ==
	/**
	 * Add an employee to the service's employees list.
	 * 
	 * @param employee the employee to be added.
	 * @throws ServiceException if the employee is already in the list or if the
	 *                          employee already is in another service.
	 */
	public void addEmployee(Employee employee) throws ServiceException {

		for (Employee emp : employees) {
			if (emp.getId() == employee.getId()) {
				throw new ServiceException("The employee already is in the service.");
			}

			if (employee.getService() != this && employee.getService() != null) {
				employee.getService().removeEmployee(employee);
			}
		}

		employees.add(employee);
		employee.setService(this);
		this.setWorkforce();
	}

	/**
	 * Add a manager to the service's employees list without setting them as manager
	 * of the service.
	 * 
	 * @param manager the manager to be added.
	 * @throws Exception if the manager already is in the list or if the manager
	 *                   already is in another service.
	 */
	public void addEmployee(Manager manager) throws ServiceException {

		if (!manager.isManaging()) {

			for (Employee emp : employees) {
				if (emp.getId() == manager.getId()) {
					throw new ServiceException("The manager already is in the service.");
				}

				if (manager.getService() != this && manager.getService() != null) {
					manager.getService().removeEmployee(manager);
				}
			}
		} else {
			LOGGER.info("The manager already is in charge of another service.");
		}

		employees.add(manager);
		setWorkforce();
		manager.setService(this);
	}

	/**
	 * Used to remove an employee from the service.
	 * 
	 * @param employee The employee to remove.
	 */
	public void removeEmployee(Employee employee) {
		employees.remove(employee);
		employee.setService(null);
		setWorkforce();
	}

	/**
	 * Used to change the current manager with another.
	 * 
	 * @param manager
	 * @throws ServiceException exception thrown by the setManager(Manager
	 *                          manager) method, please refer to it.
	 */
	public void changeManager(Manager manager) throws ServiceException {

		if (isManaged) {
			this.manager.setManagingFalse();
			this.manager = null;
			this.isManaged = false;
		}

		setManager(manager);
	}
}
