package fr.polytech.pointeuse.model.company;

public class EmployeeException extends Exception {

	// == Attributes ==
	private static final long serialVersionUID = -4455158590884788416L;
	// ================

	// == Constructors ==
	public EmployeeException(String str) {
		super(str);
	}
	// ==================
}
