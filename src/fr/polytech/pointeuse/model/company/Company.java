package fr.polytech.pointeuse.model.company;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Company class represents a company. It contains the company's name, its
 * postal code, a list of services and its owner (boss). It also gives an
 * information about the total work force capacity which is recalculated every
 * time a modification occurs.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

@Entity
public class Company implements Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	@Id
	private int id;
	private String cName;
	private String postalAddress;
	private int totalWorkforce;
	@Transient
	private Boss boss;
	@Transient
	private ArrayList<Service> services;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created Company object so that it represents an empty
	 * object. Note that this constructor is not really useful and should not be
	 * used.
	 */
	public Company() {
		this("N/A", "N/A");
	}

	/**
	 * Initializes a newly created Company objects from another Company object.
	 * 
	 * @param company the company to be copied.
	 */
	public Company(Company company) {
		this(company.cName, company.getPostalAddress());
		this.services = new ArrayList<>(company.getServices());
		this.boss = company.getBoss();
		this.totalWorkforce = company.getTotalWorkforce();
	}

	/**
	 * Initializes a newly created Company object so that it represents a company.
	 * The created company has a name and a postal code. Note that, when created,
	 * the company does not have any service nor boss.
	 * 
	 * @param cName    the company's name.
	 * @param pAddress the company's postal address.
	 */
	public Company(String cName, String pAddress) {
		this.services = new ArrayList<>();
		this.totalWorkforce = 0;
		this.cName = cName;
		this.postalAddress = pAddress;
	}
	// ==================

	// == Getters ==
	public String getcName() {
		return cName;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public List<Service> getServices() {
		return services;
	}

	public int getTotalServicesNumber() {
		return services.size();
	}

	public Boss getBoss() {
		return boss;
	}

	public int getTotalWorkforce() {
		return totalWorkforce;
	}
	// =============

	// == Setters ==
	public void setcName(String cName) {
		this.cName = cName;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public void setBoss(Boss boss) {
		this.boss = boss;
	}
	// =============

	// == Other methods ==
	/**
	 * Add a service to the services list.
	 * 
	 * @param service the service to be added.
	 * @throws Exception the service already exists in the list.
	 */
	public void addService(Service service) throws CompanyException {

		for (Service serv : services) {
			// Check if the service already exists
			if (service.getServiceName().compareToIgnoreCase(serv.getServiceName()) == 0) {
				throw new CompanyException("The service already exists in the list.");
			}
		}

		services.add(service);
		service.setCompany(this);
		this.calculateTotalWorkforce();
	}

	/**
	 * Remove a service from the services list.
	 * 
	 * @param service the service to be removed.
	 */
	public void removeService(Service service) {
		
		// Clear the service before removing it
		for (Employee emp : service.getEmployees()) {
			service.removeEmployee(emp);
		}

		services.remove(service);
		service.setCompany(null);
		this.calculateTotalWorkforce();
	}

	/**
	 * Calculates the total work force of the company, counting all employees of all
	 * services.
	 */
	public void calculateTotalWorkforce() {
		this.totalWorkforce = 0;

		for (Service serv : services) {
			this.totalWorkforce += serv.getWorkforce();
		}
	}
	// ===================
}
