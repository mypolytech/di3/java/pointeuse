package fr.polytech.pointeuse.model.company;

import java.io.Serializable;

/**
 * The SimpleEmployee class represents a light version of the Employee class. It
 * contains the employee's ID, name and service. It is used when an employee
 * clock-in/clock-out to reduce the size of the package sent. The main
 * application then create a Clocked object in order to store the informations.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 *
 */

public class SimpleEmployee implements Comparable<SimpleEmployee>, Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private int id;
	private String employeeName;
	private String employeeService;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created SimpleEmployee object so that it represents an
	 * empty object. Note that this constructor is not really useful and should not
	 * be used.
	 */
	public SimpleEmployee() {
		this(0, null, null);
	}

	/**
	 * Initializes a newly created SimpleEmployee object so that it represents a
	 * light employee object. The created simple employee has an id, a name and a
	 * service.
	 * 
	 * @param id the employee's ID.
	 * @param name the employee's name.
	 * @param service the employee's service.
	 */
	public SimpleEmployee(int id, String name, String service) {
		this.id = id;
		this.employeeName = name;
		this.employeeService = service;
	}

	/**
	 * Initializes a newly created SimpleEmployee object so that it represents a
	 * light employee object. To simplify the usage of this object, an Employee
	 * object can be used.
	 * 
	 * @param employee the employee who clocked.
	 */
	public SimpleEmployee(Employee employee) {
		this.id = employee.getId();
		this.employeeName = (employee.getFirstName() + " " + employee.getLastName());
		if (employee.getService().getServiceName() != null) {
			this.employeeService = employee.getService().getServiceName();
		} else {
			this.employeeService = "Aucun";
		}
	}
	// ==================

	// == Getters ==
	public int getID() {
		return id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public String getEmployeeService() {
		return employeeService;
	}
	// =============

	// == Other methods ==
	@Override
	public int compareTo(SimpleEmployee simEmp) {
		return employeeName.compareTo(simEmp.getEmployeeName());
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	// ===================

}
