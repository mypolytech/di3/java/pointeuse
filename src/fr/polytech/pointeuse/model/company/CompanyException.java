package fr.polytech.pointeuse.model.company;

public class CompanyException extends Exception {

	// == Attributes ==
	private static final long serialVersionUID = 7266894126061856914L;
	// ================

	// == Constructors ==
	public CompanyException(String str) {
		super(str);
	}
	// ==================
}
