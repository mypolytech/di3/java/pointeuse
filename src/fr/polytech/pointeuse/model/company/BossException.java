package fr.polytech.pointeuse.model.company;

public class BossException extends Exception {

	// == Attributes ==
	private static final long serialVersionUID = 2755605564859522979L;
	// ================

	// == Constructors ==
	public BossException(String str) {
		super(str);
	}
	// ==================
}
