package fr.polytech.pointeuse.model.company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.polytech.pointeuse.model.time.Clocked;
import fr.polytech.pointeuse.model.time.RoundedDateTime;
import fr.polytech.pointeuse.model.time.RoundedTime;

import javax.persistence.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * The Employee class represents an employee. It contains the employee's name,
 * their work schedule, their service and the list of worked days.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Employee implements Comparable<Employee>, Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(Employee.class);
	private static int newID = 1;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String firstName;
	private String lastName;
	@Transient
	private RoundedTime timeIn;
	@Transient
	private RoundedTime timeOut;

	private Integer timeBalance;
	@Transient
	private Service service;

	@Transient
	private ArrayList<Clocked> workingDays;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created Employee object so that it represents an empty
	 * object. Note that this constructor is not really useful and should not be
	 * used.
	 */
	public Employee() {
		this("N/A", "N/A", null, null);
	}

	/**
	 * Initializes a newly created Employee object so that it represents an
	 * employee. The created employee has a first name and a last name. Note that
	 * this constructor does not give a work schedule to the employee and could
	 * create errors in the future.
	 * 
	 * @param fname the employee's first name.
	 * @param lname the employee's last name.
	 */
	public Employee(String fname, String lname) {
		this(fname, lname, null, null);
	}

	/**
	 * Initializes a newly created Employee object so that it represents an
	 * employee. The created employee has a first name and a last name and a work
	 * schedule (represented by in and out).
	 * 
	 * @param fname the employee's first name.
	 * @param lname the employee's last name.
	 * @param in    the time when the employee starts working.
	 * @param out   the time when the employee stops working.
	 */
	public Employee(String fname, String lname, RoundedTime in, RoundedTime out) {
		this.id = getNewID();
		this.firstName = fname;
		this.lastName = lname;
		this.timeIn = in;
		this.timeOut = out;
		this.timeBalance = 0;
		this.workingDays = new ArrayList<>();

		++newID;
	}
	// ==================
	
	// == Getters ==
	public static int getNewID() {
		return newID;
	}
	
	public int getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public RoundedTime getTimeIn() {
		return timeIn;
	}
	
	public RoundedTime getTimeOut() {
		return timeOut;
	}
	
	public int getTimeBalance() {
		return timeBalance;
	}

	public Service getService() {
		return service;
	}

	public List<Clocked> getWorkingDays() {
		return workingDays;
	}
	// =============
	
	// == Setters ==
	public static void setNewID(Integer id) {
		newID = id;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setTimeIn(RoundedTime timeIn) {
		this.timeIn = timeIn;
	}
	
	public void setTimeOut(RoundedTime timeOut) {
		this.timeOut = timeOut;
	}
	
	public void setService(Service service) {
		this.service = service;
	}
	// =============

	// == Other methods ==
	/**
	 * Relocates the employee from a service to another.
	 * 
	 * @param service the new employee's service.
	 */
	public void changeService(Service service) {
		try {
			service.addEmployee(this);
		} catch (ServiceException serviceException) {
			LOGGER.error(serviceException.getMessage());
		}
	}

	/**
	 * Add a working day to the working days' list.
	 * 
	 * @param workingDay the working day to be added.
	 */
	public void addWorkingDay(Clocked workingDay) {
		workingDays.add(workingDay);
		workingDay.setEmployee(this);
	}

	/**
	 * Add a clock-in to an existing working day.
	 * 
	 * @param roundedDateTime the time when the employee arrived.
	 * @throws Exception the employee already clocked-in.
	 */
	public void addClockIn(RoundedDateTime roundedDateTime) throws EmployeeException {

		for (Clocked clocks : workingDays) {
			if (clocks.getDateTimeClockIn().getRoundedDate().equals(roundedDateTime.getRoundedDate())) {
				if (clocks.getDateTimeClockIn() != null) {
					throw new EmployeeException("The employee already cloked-in.");
				} else {
					clocks.setDateTimeClockIn(roundedDateTime);
				}
			}
		}
	}

	/**
	 * Add a clock-out to an existing working day.
	 * 
	 * @param roundedDateTime the time when the employee left.
	 * @throws Exception the employee already clocked-out.
	 */
	public void addClockOut(RoundedDateTime roundedDateTime) throws EmployeeException {

		for (Clocked clocks : workingDays) {
			if (clocks.getDateTimeClockOut().getRoundedDate().equals(roundedDateTime.getRoundedDate())) {
				if (clocks.getDateTimeClockOut() != null) {
					throw new EmployeeException("The employee already clocked-out.");
				} else {
					clocks.setDateTimeClockOut(roundedDateTime);
				}
			}
		}
	}

	@Override
	public String toString() {
		return (firstName + " " + lastName);
	}

	@Override
	public int compareTo(Employee emp) {
		int tmpId = emp.getId();

		return this.id - tmpId;
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	// ===================
}
