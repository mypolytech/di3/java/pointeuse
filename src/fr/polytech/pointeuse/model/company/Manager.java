package fr.polytech.pointeuse.model.company;

import fr.polytech.pointeuse.model.time.RoundedTime;

import javax.persistence.Entity;

/**
 * The Manager class represents a manager and extends the Employee class. It
 * contains the manager's e-mail and tells if the manager manages a service or
 * not.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

@Entity
public class Manager extends Employee {
	
	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private boolean isManaging = false;
	private String email;
	// ================	
	
	// == Constructors ==
	/**
	 * Initializes a newly created Manager object so that it represents an empty
	 * object. Note that this constructor is not really useful and should not be
	 * used.
	 */
	public Manager () {
		this("N/A", "N/A", null, null, "N/A");
	}
	
	/**
	 * Initializes a newly created Manager object so that it represents manager. The
	 * created manager has a first name and a last name. Note that this constructor
	 * does not give a work schedule to the manager and could create errors in the
	 * future.
	 * 
	 * @param fname the manager's first name.
	 * @param lname the manager's last name.
	 */
	public Manager (String fname, String lname) {
		this (fname, lname, null, null, "N/A");
	}
	
	/**
	 * Initializes a newly created Manager object so that it represents a manager.
	 * The created manager has a first name and a last name and a work schedule
	 * (represented by in and out).
	 * 
	 * @param fname   the manager's first name.
	 * @param lname   the manager's last name.
	 * @param timeIn  the time when the manager starts working.
	 * @param timeOut the time when the manager stops working.
	 */
	public Manager (String fname, String lname, RoundedTime timeIn, RoundedTime timeOut) {
		this (fname, lname, timeIn, timeOut, "N/A");
	}
	
	/**
	 * Initializes a newly created Employee object so that it represents an
	 * employee. The created employee has a first name and a last name and a work
	 * schedule (represented by in and out) and the e-mail. Note that a newly
	 * created manager does not manage a service.
	 * 
	 * @param fname   the manager's first name.
	 * @param lname   the manager's last name.
	 * @param timeIn  the time when the manager starts working.
	 * @param timeOut the time when the manager stops working.
	 * @param email   the manager's email.
	 */
	public Manager (String fname, String lname, RoundedTime timeIn, RoundedTime timeOut, String email) {
		super (fname, lname, timeIn, timeOut);
		this.email = email;
	}
	
	// == Getters ==
	public boolean isManaging() {
		return isManaging;
	}

	public String getEmail () {
		return this.email;
	}
	// =============
	
	// == Setters ==
	public void setManagingTrue() {
		this.isManaging = true;
	}
	
	public void setManagingFalse() {
		this.isManaging = false;
	}

	public void setEmail (String email) {
		this.email = email;
	}
	// =============
	
	// == Other methods ==
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	// ===================
}
