package fr.polytech.pointeuse.model.company;

public class ServiceException extends Exception {
	
	// == Attributes ==
	private static final long serialVersionUID = 1L;
	// ================
	
	// == Constructors ==
	public ServiceException(String str) {
		super(str);
	}
	// ==================
}
