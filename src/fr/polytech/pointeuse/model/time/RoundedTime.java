package fr.polytech.pointeuse.model.time;

import java.io.Serializable;
import java.time.LocalTime;

/**
 * The RoundedTime class represents a rounded time. The rules to round the time
 * are the following :
 * <ul>
 * <li>The minutes are only multiple of 15 minutes
 * <li>Below 7 minutes : rounded to 0
 * <li>Between 7 and 21 : rounded to 15
 * <li>Between 21 and 37 : rounded to 30
 * <li>Between 37 and 51 : rounded to 45
 * <li>Above 51 minutes : rounded to 0 and hours+ 1
 * </ul>
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

public class RoundedTime implements Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private LocalTime roundedTime;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created RoundedTime object so that it represents a
	 * rounded time with the current time system. See the class description for the
	 * rounded time rules.
	 */
	public RoundedTime() {
		roundedTime = LocalTime.now();

		if (roundedTime.getMinute() <= 7) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
		if (roundedTime.getMinute() > 7 && roundedTime.getMinute() <= 21) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 15, 0, 0);
		}
		if (roundedTime.getMinute() > 21 && roundedTime.getMinute() <= 37) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 30, 0, 0);
		}
		if (roundedTime.getMinute() > 37 && roundedTime.getMinute() <= 51) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 45, 0, 0);
		}
		if (roundedTime.getMinute() > 51) {
			roundedTime = roundedTime.plusHours(1);
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
	}

	/**
	 * Initializes a newly created RoundedTime object so that it represents a
	 * rounded time with the input time. See the class description for the rounded
	 * time rules.
	 *
	 * @param lt the time to be rounded.
	 */
	public RoundedTime(LocalTime lt) {
		roundedTime = lt;

		if (roundedTime.getMinute() <= 7) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
		if (roundedTime.getMinute() > 7 && roundedTime.getMinute() <= 21) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 15, 0, 0);
		}
		if (roundedTime.getMinute() > 21 && roundedTime.getMinute() <= 37) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 30, 0, 0);
		}
		if (roundedTime.getMinute() > 37 && roundedTime.getMinute() <= 51) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 45, 0, 0);
		}
		if (roundedTime.getMinute() > 51) {
			roundedTime = roundedTime.plusHours(1);
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
	}
	// ==================

	// == Getters ==
	public LocalTime getRoundedTime() {
		return roundedTime;
	}
	// =============

	// == Setters ==
	public void setRoundedTime(LocalTime lt) {
		roundedTime = lt;

		if (roundedTime.getMinute() <= 7) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
		if (roundedTime.getMinute() > 7 && roundedTime.getMinute() <= 21) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 15, 0, 0);
		}
		if (roundedTime.getMinute() > 21 && roundedTime.getMinute() <= 37) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 30, 0, 0);
		}
		if (roundedTime.getMinute() > 37 && roundedTime.getMinute() <= 51) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 45, 0, 0);
		}
		if (roundedTime.getMinute() > 51) {
			roundedTime = roundedTime.plusHours(1);
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
	}
	// =============

	@Override
	public String toString() {
		return roundedTime.toString();
	}
}
