package fr.polytech.pointeuse.model.time;

public class ClockedException extends Exception {

	// == Attributes ==
	private static final long serialVersionUID = -5182871085227057214L;
	// ================
	
	// == Constructors ==
	public ClockedException(String msg) {
		super(msg);
	}
	// ==================
}
