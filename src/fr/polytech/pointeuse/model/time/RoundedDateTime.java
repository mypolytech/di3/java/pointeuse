package fr.polytech.pointeuse.model.time;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * The RoundedDateTime class represents a rounded DateTime object. The rules to
 * round the time are the following :
 * <ul>
 * <li>The minutes are only multiple of 15 minutes
 * <li>Below 7 minutes : rounded to 0
 * <li>Between 7 and 21 : rounded to 15
 * <li>Between 21 and 37 : rounded to 30
 * <li>Between 37 and 51 : rounded to 45
 * <li>Above 51 minutes : rounded to 0 and hours+ 1
 * </ul>
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

public class RoundedDateTime implements Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private LocalTime roundedTime;
	private LocalDate roundedDate;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created RoundeddateTime object so that it represents a
	 * rounded DateTime with the current time system. See the class description for
	 * the rounded time rules.
	 */
	public RoundedDateTime() {
		LocalDateTime ldt = LocalDateTime.now();
		roundedTime = ldt.toLocalTime();
		roundedDate = ldt.toLocalDate();

		if (roundedTime.getMinute() <= 7) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
		if (roundedTime.getMinute() > 7 && roundedTime.getMinute() <= 21) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 15, 0, 0);
		}
		if (roundedTime.getMinute() > 21 && roundedTime.getMinute() <= 37) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 30, 0, 0);
		}
		if (roundedTime.getMinute() > 37 && roundedTime.getMinute() <= 51) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 45, 0, 0);
		}
		if (roundedTime.getMinute() > 51) {
			if (roundedTime.getHour() == 23) {
				roundedDate = roundedDate.plusDays(1);
			}
			roundedTime = roundedTime.plusHours(1);
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
	}

	/**
	 * Initializes a newly created RoundedDateTime object so that it represents a
	 * rounded DateTime with the input time. See the class description for the
	 * rounded time rules.
	 * 
	 * @param ldt the time to be rounded.
	 */
	public RoundedDateTime(LocalDateTime ldt) {
		roundedTime = ldt.toLocalTime();
		roundedDate = ldt.toLocalDate();

		if (roundedTime.getMinute() <= 7) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
		if (roundedTime.getMinute() > 7 && roundedTime.getMinute() <= 21) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 15, 0, 0);
		}
		if (roundedTime.getMinute() > 21 && roundedTime.getMinute() <= 37) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 30, 0, 0);
		}
		if (roundedTime.getMinute() > 37 && roundedTime.getMinute() <= 51) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 45, 0, 0);
		}
		if (roundedTime.getMinute() > 51) {
			if (roundedTime.getHour() == 23) {
				roundedDate = roundedDate.plusDays(1);
			}
			roundedTime = roundedTime.plusHours(1);
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
	}
	// ==================

	// == Getters ==
	public LocalTime getRoundedTime() {
		return roundedTime;
	}

	public LocalDate getRoundedDate() {
		return roundedDate;
	}

	public LocalDateTime getRoundedDateTime() {
		return LocalDateTime.of(roundedDate, roundedTime);
	}
	// =============

	// == Setters ==
	public void setRoundedDateTime(LocalDateTime ldt) {
		roundedTime = ldt.toLocalTime();
		roundedDate = ldt.toLocalDate();
		if (roundedTime.getMinute() > 51) {
			if (roundedTime.getHour() == 23)
				roundedDate = roundedDate.plusDays(1);
		}
		setRoundedTime(roundedTime);
	}

	public void setRoundedTime(LocalTime lt) {
		roundedTime = lt;

		if (roundedTime.getMinute() <= 7) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
		if (roundedTime.getMinute() > 7 && roundedTime.getMinute() <= 21) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 15, 0, 0);
		}
		if (roundedTime.getMinute() > 21 && roundedTime.getMinute() <= 37) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 30, 0, 0);
		}
		if (roundedTime.getMinute() > 37 && roundedTime.getMinute() <= 51) {
			roundedTime = LocalTime.of(roundedTime.getHour(), 45, 0, 0);
		}
		if (roundedTime.getMinute() > 51) {
			roundedTime = roundedTime.plusHours(1);
			roundedTime = LocalTime.of(roundedTime.getHour(), 0, 0, 0);
		}
	}

	public void setRoundedDate(LocalDate ld) {
		roundedDate = ld;
	}
	// =============

	@Override
	public String toString() {
		return LocalDateTime.of(roundedDate, roundedTime).toString();
	}
}
