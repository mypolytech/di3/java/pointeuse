package fr.polytech.pointeuse.model.time;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import fr.polytech.pointeuse.model.company.Employee;

/**
 * The Clocked class represents an employee's day of work. It contains the
 * employee, the time he clocked in/out and informations about starting or
 * ending the work day early or late.
 * 
 * @version 1.0
 * @author Yohann BENETREAULT
 * @author Olivier COUE
 * @author Maël CRENN
 * @author Timothé SANOUILLER--TOURNE
 */

public class Clocked implements Comparable<Clocked>, Serializable {

	// == Attributes ==
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(Clocked.class);
	private Employee employee;
	private RoundedDateTime dateTimeClockIn;
	private RoundedDateTime dateTimeClockOut;
	private boolean startedLate;
	private boolean startedEarly;
	private boolean leftEarly;
	private boolean leftLate;
	// ================

	// == Constructors ==
	/**
	 * Initializes a newly created Clocked object so that it represents an empty
	 * object. Note that this constructor is not really useful and should not be
	 * used.
	 */
	public Clocked() {
		this(null, null, null);
	}

	/**
	 * Initializes a newly created Clocked object so that it represents a work day
	 * for an employee. The created clocked has an employee, a time in and a time
	 * out.
	 * 
	 * @param employee the employee who clocked.
	 * @param timeIn   the time they started working.
	 * @param timeOut  the time they stopped working.
	 */
	public Clocked(Employee employee, RoundedDateTime timeIn, RoundedDateTime timeOut) {
		this.dateTimeClockIn = timeIn;
		this.dateTimeClockOut = timeOut;
		this.employee = employee;

		if (employee != null) {
			try {
				this.check();
			} catch (ClockedException clockedException) {
				LOGGER.error(clockedException.getMessage());
			}
		}
	}
	// ==================

	// == Getters ==
	public Employee getEmployee() {
		return employee;
	}

	public RoundedDateTime getDateTimeClockIn() {
		return dateTimeClockIn;
	}

	public RoundedDateTime getDateTimeClockOut() {
		return dateTimeClockOut;
	}
	// =============

	// == Setters ==
	public void setEmployee(Employee employee) {
		this.employee = employee;

		try {
			this.check();
		} catch (ClockedException clockedException) {
			LOGGER.error(clockedException.getMessage());
		}
	}

	public void setDateTimeClockIn(RoundedDateTime dateTimeClockIn) {
		this.dateTimeClockIn = dateTimeClockIn;
	}

	public void setDateTimeClockOut(RoundedDateTime dateTimeClockOut) {
		this.dateTimeClockOut = dateTimeClockOut;

		try {
			this.check();
		} catch (ClockedException clockedException) {
			LOGGER.error(clockedException.getMessage());
		}
	}
	// =============

	// == Booleans ==
	public boolean hasStartedLate() {
		return startedLate;
	}

	public boolean hasStartedEarly() {
		return startedEarly;
	}

	public boolean hasLeftEarly() {
		return leftEarly;
	}

	public boolean hasLeftLate() {
		return leftLate;
	}
	// ==============

	// == Other methods ==
	/**
	 * Does some verifications on the objects used to create a Clocked object.
	 * Checks if the employee is not null and set the booleans by checking the
	 * values of the input RoundedTime objects.
	 * 
	 * @throws ClockedException the employee is null.
	 */
	private void check() throws ClockedException {

		if (employee == null) {
			throw new ClockedException("Le pointage n'est associé à aucun employé.");
		}

		if (dateTimeClockIn.getRoundedTime().compareTo(employee.getTimeIn().getRoundedTime()) < 0) {
			this.startedEarly = true;
		} else if (dateTimeClockIn.getRoundedTime().compareTo(employee.getTimeIn().getRoundedTime()) > 0) {
			this.startedLate = true;
		}

		if (dateTimeClockOut.getRoundedTime().compareTo(employee.getTimeOut().getRoundedTime()) < 0) {
			this.leftEarly = true;
		} else if (dateTimeClockOut.getRoundedTime().compareTo(employee.getTimeOut().getRoundedTime()) > 0) {
			this.leftLate = true;
		}

	}

	@Override
	public int compareTo(Clocked clocked) {
		if (dateTimeClockIn != null) {
			if (clocked.dateTimeClockIn != null)
				return LocalDateTime.of(dateTimeClockIn.getRoundedDate(), dateTimeClockIn.getRoundedTime())
						.compareTo(LocalDateTime.of(clocked.dateTimeClockIn.getRoundedDate(),
								clocked.dateTimeClockIn.getRoundedTime()));
			else if (clocked.dateTimeClockOut != null)
				return LocalDateTime.of(dateTimeClockIn.getRoundedDate(), dateTimeClockIn.getRoundedTime())
						.compareTo(LocalDateTime.of(clocked.dateTimeClockOut.getRoundedDate(),
								clocked.dateTimeClockOut.getRoundedTime()));
			else {
				return -1;
			}
		} else if (dateTimeClockOut != null) {
			if (clocked.dateTimeClockIn != null)
				return LocalDateTime.of(dateTimeClockOut.getRoundedDate(), dateTimeClockOut.getRoundedTime())
						.compareTo(LocalDateTime.of(clocked.dateTimeClockIn.getRoundedDate(),
								clocked.dateTimeClockIn.getRoundedTime()));
			else if (clocked.dateTimeClockOut != null)
				return LocalDateTime.of(dateTimeClockOut.getRoundedDate(), dateTimeClockOut.getRoundedTime())
						.compareTo(LocalDateTime.of(clocked.dateTimeClockOut.getRoundedDate(),
								clocked.dateTimeClockOut.getRoundedTime()));
			else {
				return -1;
			}
		} else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
