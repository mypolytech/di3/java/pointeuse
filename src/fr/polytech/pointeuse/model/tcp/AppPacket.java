package fr.polytech.pointeuse.model.tcp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import fr.polytech.pointeuse.model.company.SimpleEmployee;

public class AppPacket implements Serializable {
	
	// ===== Attributes =====

	private static final long serialVersionUID = 1L;
	private ArrayList<SimpleEmployee> employeesLists;
	
	// ===== Constructors =====
	
	public AppPacket() {
		employeesLists = new ArrayList<SimpleEmployee>();
	}
	
	public AppPacket(ArrayList<SimpleEmployee> list) {
		this.employeesLists = new ArrayList<SimpleEmployee>(list);
		Collections.sort(employeesLists);
	}

	// ===== Getter =====
	
	public ArrayList<SimpleEmployee> getEmployeesList() {
		return employeesLists;
	}
	
	// ===== Setter =====
	public void setEmployeesList (ArrayList<SimpleEmployee> list) {
		employeesLists = list;
	}
}
