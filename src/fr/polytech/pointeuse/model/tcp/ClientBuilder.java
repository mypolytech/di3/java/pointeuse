package fr.polytech.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ClientBuilder {
	
	// ===== Attributes =====
	
	InetSocketAddress clientInetSocketAdress;
	Socket socketOfClient;
	ObjectInputStream input;
	ObjectOutputStream output;
	
	// ===== Constructors =====
	
	public ClientBuilder () {
		clientInetSocketAdress = new InetSocketAddress("localhost", 8085);
		socketOfClient = null;
	}
	
	public ClientBuilder (int port) {
		clientInetSocketAdress = new InetSocketAddress("localhost", port);
		socketOfClient = null;
	}
	
	// ===== Setter =====
	
	protected void setSocket() throws IOException {		
		socketOfClient = new Socket(clientInetSocketAdress.getHostName(), clientInetSocketAdress.getPort());
		socketOfClient.setSoTimeout(5000);
	}
}
