package fr.polytech.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.model.company.SimpleEmployee;

public class AppServer extends ServerBuilder implements Runnable {
	
	// ===== Attributes =====
	
	private Company comp;
	private AppPacket packet;
	
	// ===== Constructors =====
	
	public AppServer(Company comp) {
		super();
		this.comp = comp;
	}
	
	public AppServer(int port, Company comp) {
		super(port);
		this.comp = comp;
	}
	
	// ===== Setters =====
	
	public void setPacket(AppPacket packet) {
		this.packet = packet;
	}
	
	// ===== Update packet =====
	private void updatePacket() {
		ArrayList<SimpleEmployee> empList = new ArrayList<SimpleEmployee>();
		
		for (Service serv : comp.getServices()) {
			for (Employee emp : serv.getEmployees()) {
				empList.add(new SimpleEmployee(emp));
			}
		}
		
		Collections.sort(empList);
		
		packet.setEmployeesList(empList);
	}
	
	// ===== Run =====
	
	@Override
	public void run() {
		
		try {
			setSocket();
			
			while (true) {
				
				updatePacket();
				
				socketOfServer = serverSocket.accept();
				
				output = new ObjectOutputStream(socketOfServer.getOutputStream());
				output.writeObject(packet);
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}