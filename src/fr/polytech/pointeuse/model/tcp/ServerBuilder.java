package fr.polytech.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerBuilder {
	
	// ===== Attributes =====

	InetSocketAddress serverInetSocketAddress;
	ServerSocket serverSocket;
	Socket socketOfServer;
	ObjectOutputStream output;
	ObjectInputStream input;

	// ===== Constructors =====
	
	public ServerBuilder () {
		serverInetSocketAddress = new InetSocketAddress("localhost", 8085);
		serverSocket = null;
		socketOfServer = null;
	}
	
	public ServerBuilder (int port) {
		serverInetSocketAddress = new InetSocketAddress("localhost", port);
		serverSocket = null;
		socketOfServer = null;
	}
	
	// ===== Setter =====
	
	protected void setSocket() throws IOException {
		serverSocket = new ServerSocket(serverInetSocketAddress.getPort());
	}
}