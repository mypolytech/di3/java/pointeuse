package fr.polytech.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.model.serialization.Serialization;

public class EmulatorClient extends ClientBuilder implements Runnable {
	
	// ===== Attributes =====
	
	private AppPacket packet;
	
	// ===== Constructors =====
	
	public EmulatorClient() {
		super();
		packet = new AppPacket();
	}
	
	public EmulatorClient(int port) {
		super(port);
		packet = new AppPacket();
	}
	
	// ===== Getter =====
	
	public AppPacket getPacket() {
		return packet;
	}
	
	// ===== Run =====
	
	@Override
	public void run() {
		
		try {
			setSocket();
			
			input = new ObjectInputStream(socketOfClient.getInputStream());
			
			System.out.println("Lecture faite");
			
			try {
				packet = (AppPacket) input.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			input.close();
			socketOfClient.close();
			System.out.println("Client : terminé.");
		} catch (IOException e) {
			
			Serialization serial = new Serialization("./Serialisation/company.dat");
			Company comp = new Company();
			
			try {
				comp = serial.loadCompanyFromFile();
			} catch (ClassNotFoundException | IOException e1) {
				e1.printStackTrace();
			}
			
			ArrayList<SimpleEmployee> empList = new ArrayList<SimpleEmployee>();
			
			for (Service serv : comp.getServices()) {
				for (Employee emp : serv.getEmployees()) {
					empList.add(new SimpleEmployee(emp));
				}
			}
			
			Collections.sort(empList);
			
			packet.setEmployeesList(empList);
		}
		
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
