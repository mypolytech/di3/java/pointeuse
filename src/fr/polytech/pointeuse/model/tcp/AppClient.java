package fr.polytech.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectOutputStream;

public class AppClient extends ClientBuilder implements Runnable {

	// ===== Attributes =====
	
	private EmulatorPacket packet;
	
	// ===== Constructors =====
	
	public AppClient() {
		super();
		packet = new EmulatorPacket();
	}
	
	public AppClient(int port) {
		super(port);
		packet = new EmulatorPacket();
	}
	
	// ===== Getter =====
	
	public EmulatorPacket getPacket() {
		return packet;
	}
	
	// ===== Setter =====
	
	public void setPacket(EmulatorPacket packet) {
		this.packet = packet;
	}
	
	// ===== Run =====
	
	@Override
	public void run() {
		
		try {
			setSocket();
			
			output = new ObjectOutputStream(socketOfClient.getOutputStream());
			output.writeObject(packet);
			
			output.close();
			socketOfClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
