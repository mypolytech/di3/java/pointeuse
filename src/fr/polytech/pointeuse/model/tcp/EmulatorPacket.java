package fr.polytech.pointeuse.model.tcp;

import fr.polytech.pointeuse.model.time.RoundedDateTime;
import java.io.Serializable;

public class EmulatorPacket implements Serializable {

	// ===== Attributes =====
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private RoundedDateTime roundedDateTime;
	private String inOrOut;
	
	// ===== Constructors =====
	
	public EmulatorPacket() {
		this(0, null, null);
	}
	
	public EmulatorPacket(Integer id, RoundedDateTime roundedDateTime, String inOrOut) {
		this.id = id;
		this.roundedDateTime = roundedDateTime;
		this.inOrOut = inOrOut;
	}
	
	// ===== Getters =====

	public Integer getId() {
		return id;
	}

	public RoundedDateTime getRoundedDateTime() {
		return roundedDateTime;
	}
	
	public String getInOrOut() {
		return inOrOut;
	}
}
