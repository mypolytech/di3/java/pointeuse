package fr.polytech.pointeuse.model.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.model.time.Clocked;
import fr.polytech.pointeuse.view.emulator.EmulatorErrorBox;

public class EmulatorServer extends ServerBuilder implements Runnable {

	// ===== Attributes =====
	
	private Company comp;
	private EmulatorPacket packet;
	
	// ===== Constructors =====
	
	public EmulatorServer(Company comp) {
		super();
		this.comp = comp;
	}
	
	public EmulatorServer(int port, Company comp) {
		super(port);
		this.comp = comp;
	}
	
	// ===== Setter =====
	
	public void setPacket(EmulatorPacket packet) {
		this.packet = packet;
	}
	
	// ===== Run =====
	
	@Override
	public void run() {
		
		try {
			setSocket();
				
			while (true) {
				socketOfServer = serverSocket.accept();
				
				input = new ObjectInputStream(socketOfServer.getInputStream());
				
				try {
					packet = (EmulatorPacket) input.readObject();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				
				System.out.println("Packet received :");
				System.out.println(packet.getId());
				System.out.println(packet.getRoundedDateTime());
				System.out.println(packet.getInOrOut());
				
				// Creating the working day and update the employees working days
				for (Service serv : comp.getServices()) {
					for (Employee emp : serv.getEmployees()) {
						if (emp.getId() == packet.getId()) {
							if (packet.getInOrOut().equals("in")) {
								try {
									Clocked clock = new Clocked(emp, packet.getRoundedDateTime(), null);
									emp.addWorkingDay(clock);
								} catch (Exception e) {
									try {
										emp.addClockIn(packet.getRoundedDateTime());
									} catch (Exception e1) {
										@SuppressWarnings("unused")
										EmulatorErrorBox emulatorErrorBox = new EmulatorErrorBox("L'employé a déjà pointé en arrivant.");
									}
								}							
							} else if (packet.getInOrOut().equals("out")) {
								try {
									Clocked clock = new Clocked(emp, null, packet.getRoundedDateTime());
									emp.addWorkingDay(clock);
								} catch (Exception e) {
									try {
										emp.addClockOut(packet.getRoundedDateTime());
									} catch (Exception e1) {
										@SuppressWarnings("unused")
										EmulatorErrorBox emulatorErrorBox = new EmulatorErrorBox("L'employé a déjà pointé en partant.");
									}
								}
							}
						}
					}
				}
//				
//				// Update the history table in the main app
//				// TODO
//				for (Service serv : comp.getServices()) {
//					for (Employee emp : serv.getEmployees()) {
//						System.out.println(emp.getLastName() + " " + emp.getFirstName());
//						for(Clocked clock : emp.getWorkingDays()) {
//							System.out.println(clock.getDate().toString() + " In : " + clock.getTimeClockIn().toString() +  " / Out : " + clock.getTimeClockOut().toString());
//						}
//					}
//				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
