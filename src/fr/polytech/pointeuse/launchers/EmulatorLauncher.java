package fr.polytech.pointeuse.launchers;

import java.io.IOException;
import java.util.ArrayList;

import fr.polytech.pointeuse.controller.emulator.EmulatorController;
import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.model.tcp.EmulatorClient;
import fr.polytech.pointeuse.view.emulator.EmulatorWindow;

public class EmulatorLauncher {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		ArrayList<SimpleEmployee> simpleEmployeesList = new ArrayList<SimpleEmployee>();
		
		@SuppressWarnings("unused")
		EmulatorWindow emu = new EmulatorWindow(simpleEmployeesList);
		
		EmulatorClient emuClient = new EmulatorClient(8081);
		
		Thread sync = new Thread(emuClient);
		sync.start();
			
		Thread emuSync = EmulatorController.createEmulatorSync(emuClient);
		emuSync.start();
	}
}
