package fr.polytech.pointeuse.launchers;

import java.io.IOException;
import java.util.ArrayList;

import fr.polytech.pointeuse.model.company.*;
import fr.polytech.pointeuse.model.serialization.Serialization;
import fr.polytech.pointeuse.model.tcp.AppPacket;
import fr.polytech.pointeuse.model.tcp.AppServer;
import fr.polytech.pointeuse.model.tcp.EmulatorServer;
import fr.polytech.pointeuse.utils.HibernateUtil;
import fr.polytech.pointeuse.view.app.AppWindow;

public class AppLauncher {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		Serialization serialization = new Serialization("./Serialisation/company.dat");

		Company comp = serialization.loadCompanyFromFile();
		HibernateUtil.getSessionFactory();
		ArrayList<SimpleEmployee> simpleEmployeesList = new ArrayList<>();
		
		for(Service serv : comp.getServices()) {
			for(Employee emp : serv.getEmployees()) {
				simpleEmployeesList.add(new SimpleEmployee(emp));
			}
		}
	
		@SuppressWarnings("unused")
		AppWindow app = new AppWindow(comp);
		
		AppServer appServer = new AppServer(8081, comp);
		EmulatorServer emuServer = new EmulatorServer(8086, comp);
		
		AppPacket packet = new AppPacket(simpleEmployeesList);
		
		Thread syncEmulator = new Thread(appServer);
		appServer.setPacket(packet);
		syncEmulator.start();
		
		Thread receiveClocks = new Thread(emuServer);
		receiveClocks.start();
	}
}
