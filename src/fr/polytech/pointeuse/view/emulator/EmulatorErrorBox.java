package fr.polytech.pointeuse.view.emulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.polytech.pointeuse.controller.app.AppController;

public class EmulatorErrorBox extends JDialog {

	private static final long serialVersionUID = 1L;

	public EmulatorErrorBox(String str) {
		
		setTitle("Erreur");
		setVisible(true);
		setSize(400, 150);
		setAlwaysOnTop(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		JPanel panel = new JPanel();
		
		JLabel msg = new JLabel();
		msg.setBounds(50, 10, 364, 44);
		msg.setText(str);
		msg.setFont(new Font("Arial", Font.BOLD, 14));
		
		JButton ok = new JButton("Ok");
		ok.setBounds(162, 60, 60, 25);
		ok.addActionListener(AppController.closeDialog(this));
		
		panel.setBackground(Color.WHITE);
		panel.setLayout(null);
		panel.add(msg);
		panel.add(ok);
		
		setContentPane(panel);
	}
}
