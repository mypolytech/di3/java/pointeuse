package fr.polytech.pointeuse.view.emulator;

import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.*;

import fr.polytech.pointeuse.controller.emulator.EmulatorController;
import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.model.time.RoundedDateTime;
import fr.polytech.pointeuse.model.time.RoundedTime;

public class EmulatorWindow extends JFrame {

	// ===== Attributes =====
	
	private static final long serialVersionUID = 1L;
	private JPanel panel = new JPanel();
	private Icon logo = new ImageIcon("./Fictice.png");
	private JLabel logoDisplay = new JLabel();
	private static JButton clockInButton = new JButton();
	private static JButton clockOutButton = new JButton();
	private static ArrayList<SimpleEmployee> employeesList = new ArrayList<>();
	private static JComboBox<String> employeesChoice = new JComboBox<>();
	private static JLabel empID = new JLabel();
	private static JLabel empName = new JLabel();
	private static JLabel empService = new JLabel();
	
	// ===== Constructors =====
	
	public EmulatorWindow(ArrayList<SimpleEmployee> employeeSimpleList) {
		
		// The list of the company employees
		employeesList = employeeSimpleList;
		
		// Get the company's logo and displays it
		logoDisplay.setBounds(285, 5, 295, 144);
		logoDisplay.setIcon(logo);
		logoDisplay.setBorder(BorderFactory.createLineBorder(Color.black));		
		
		// Builds the ComboBox
		for(SimpleEmployee simEmp : employeesList) {
			employeesChoice.addItem(simEmp.getEmployeeName());
		}
		
		employeesChoice.setBounds(40, 125, 200, 25);
		employeesChoice.addActionListener(EmulatorController.emulatorInfosUpdate(employeesList, employeesChoice));
		
		
		// Displays infos
		// By default it shows nothing, will be updated when updating the employee's list
		empID.setText("ID : ");
		empID.setBounds(40, 175, 200, 25);
		empID.setFont(new Font("Arial", Font.BOLD, 14));
		
		empName.setText("Nom : ");
		empName.setBounds(40, 200, 200, 25);
		empName.setFont(new Font("Arial", Font.BOLD, 14));
		
		empService.setText("Département : ");
		empService.setBounds(40, 225, 300, 25);
		empService.setFont(new Font("Arial", Font.BOLD, 14));
		
		// Displays the current date
		JLabel date = new JLabel();
		date.setText("Date : " + DateFormat.getDateInstance().format(new Date()));
		date.setFont(new Font("Arial", Font.BOLD, 18));
		date.setBounds(40, 25, 200, 25);
		
		// Displays the current time
		JLabel time = new JLabel();
		time.setText("Heure " + DateFormat.getTimeInstance().format(new Date()) + "(" + new RoundedTime(LocalTime.now()) + ")");
		time.setFont(new Font("Arial", Font.BOLD, 18));
		time.setBounds(40, 50, 200, 25);
		
		Timer timer = new Timer(500, e -> {
			date.setText("Date : " + DateFormat.getDateInstance().format(new Date()));
			time.setText("Heure " + DateFormat.getTimeInstance().format(new Date()) + " (" + new RoundedTime(LocalTime.now()) + ")");
		});
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
        
        // ClockIn button setup
        clockInButton.setText("Arrivée");
        clockInButton.setBounds(350, 185, 100, 40);

        // ClockOut button setup
        clockOutButton.setText("Départ");
        clockOutButton.setBounds(480, 185, 100, 40);
		
        // Frame setup
		setTitle("Time tracker emulator");
		setSize(600, 300);
		setLocation(25, 300);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Panel setup
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.add(logoDisplay);
		panel.add(employeesChoice);
		panel.add(empID);
		panel.add(empName);
		panel.add(empService);
		panel.add(date);
		panel.add(time);
		panel.add(clockInButton);
		panel.add(clockOutButton);
		
		// Add the panel to the frame
		setContentPane(panel);
	}
	
	// ===== Other methods =====
	
	/**
	 * Set the emulator's simple employees list.
	 * It is used every time the emulator update its employee's list.
	 * @param employeeList The new employee's list.
	 */
	public static void setEmployeesList(ArrayList<SimpleEmployee> employeeList) {
		employeesList.clear();
		employeesList = employeeList;
	}
	
	/**
	 * Get the emulator's simple employees list.
	 * It is used every time the emulator update its employee's list.
	 * @return The emulator's simple employees list.
	 */
	public static ArrayList<SimpleEmployee> getEmpList () {
		return employeesList;
	}
	
	/**
	 * Updates the ComboBox and the infos.
	 * @param empList The simple employees list to use.
	 */
	public static void updateEmployeesList(ArrayList<SimpleEmployee> empList) {
		
		employeesChoice.removeAll();
		
		Collections.sort(empList);
		setEmployeesList(empList);
		
		System.out.println("Updating...");
		
		for (SimpleEmployee simEmp : empList) {
			employeesChoice.addItem(simEmp.getEmployeeName());
		}
		
		updateInfosDisplayed(empList.get(0).getID(), empList.get(0).getEmployeeName(), empList.get(0).getEmployeeService());
		
		employeesChoice.addActionListener(EmulatorController.emulatorInfosUpdate(employeesList, employeesChoice));
		clockInButton.addActionListener(EmulatorController.emulatorSendClockIn(employeesChoice, employeesList, LocalDate.now(), new RoundedDateTime(LocalDateTime.now())));
		clockOutButton.addActionListener(EmulatorController.emulatorSendClockOut(employeesChoice, employeesList, LocalDate.now(),  new RoundedDateTime(LocalDateTime.now())));
	}
	
	/**
	 * Update the displayed infos about the selected employee.
	 * @param ID The employee's ID.
	 * @param fullName The employee's last name and first name.
	 * @param service The employee's service.
	 */
	public static void updateInfosDisplayed(Integer ID, String fullName, String service) {
		
		empID.setText("ID : " + ID);
		empName.setText("Nom : " + fullName);
		empService.setText("Département : " + service);
	}
}