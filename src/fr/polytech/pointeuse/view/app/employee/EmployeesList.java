package fr.polytech.pointeuse.view.app.employee;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;

class EmployeesList extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private final ArrayList<Employee> employeesList;
    private final String[] tableHeaders = {"ID", "Nom", "Prénom", "Début", "Fin", "Balance", "Département"};

    public EmployeesList(Company company) {

        super();

        employeesList = new ArrayList<>();

        for (Service serv : company.getServices()) {
            employeesList.addAll(serv.getEmployees());
        }

        Collections.sort(employeesList);
    }

    public int getRowCount() {
        return employeesList.size();
    }

    public int getColumnCount() {
        return tableHeaders.length;
    }

    public String getColumnName(int columnIndex) {
        return tableHeaders[columnIndex];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {

            case 0:
                return employeesList.get(rowIndex).getId();

            case 1:
                return employeesList.get(rowIndex).getLastName();

            case 2:
                return employeesList.get(rowIndex).getFirstName();

            case 3:
                return employeesList.get(rowIndex).getTimeIn().toString();

            case 4:
                return employeesList.get(rowIndex).getTimeOut().toString();

            case 5:
                return employeesList.get(rowIndex).getTimeBalance();

            case 6:
                return employeesList.get(rowIndex).getService().getServiceName();

            default:
                return null;
        }
    }
}