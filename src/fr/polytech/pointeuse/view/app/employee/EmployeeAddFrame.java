package fr.polytech.pointeuse.view.app.employee;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Service;

public class EmployeeAddFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public EmployeeAddFrame(Company comp) {
		
		JPanel panel = new JPanel();
		
		JLabel lastNameFieldTitle = new JLabel();
		lastNameFieldTitle.setText("Nom");
		lastNameFieldTitle.setBounds(25, 15, 200, 25);
		lastNameFieldTitle.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField employeeLastName = new JTextField();
		employeeLastName.setBounds(25, 40, 150, 25);
		employeeLastName.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel firstNameFieldTitle = new JLabel();
		firstNameFieldTitle.setText("Prénom");
		firstNameFieldTitle.setBounds(190, 15, 200, 25);
		firstNameFieldTitle.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField employeeFirstName = new JTextField();
		employeeFirstName.setBounds(190, 40, 150, 25);
		employeeFirstName.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JComboBox<String> serviceChoice = new JComboBox<String>();
		
		for (Service serv : comp.getServices()) {
			serviceChoice.addItem(serv.getServiceName());
		}
		
		serviceChoice.setBounds(25, 110, 200, 25);
		
		JLabel serviceChoiceTitle = new JLabel();
		serviceChoiceTitle.setText("Choix du service");
		serviceChoiceTitle.setFont(new Font("Arial", Font.BOLD, 14));
		serviceChoiceTitle.setBounds(25, 85, 200, 25);
		
		JButton confirm = new JButton();
		confirm.setText("Ajouter");
		confirm.setBounds(75, 160, 80, 40);
		
		JButton cancel = new JButton();
		cancel.setText("Annuler");
		cancel.setBounds(220, 160, 80, 40);
		cancel.addActionListener(AppController.closeFrame(this));
		
		setTitle("Ajout d'employé");
		setSize(375, 250);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setVisible(true);
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.add(lastNameFieldTitle);
		panel.add(employeeLastName);
		panel.add(firstNameFieldTitle);
		panel.add(employeeFirstName);
		panel.add(serviceChoice);
		panel.add(serviceChoiceTitle);
		panel.add(confirm);
		panel.add(cancel);
		
		setContentPane(panel);
	}
}
