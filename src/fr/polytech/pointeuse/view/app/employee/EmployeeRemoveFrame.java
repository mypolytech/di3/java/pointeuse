package fr.polytech.pointeuse.view.app.employee;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;

public class EmployeeRemoveFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public EmployeeRemoveFrame(Company comp) {

		JPanel panel = new JPanel();
		
		JLabel lastNameFieldTitle = new JLabel();
		lastNameFieldTitle.setText("Nom");
		lastNameFieldTitle.setBounds(25, 15, 200, 25);
		lastNameFieldTitle.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField employeeLastName = new JTextField();
		employeeLastName.setBounds(25, 40, 150, 25);
		employeeLastName.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel firstNameFieldTitle = new JLabel();
		firstNameFieldTitle.setText("Prénom");
		firstNameFieldTitle.setBounds(190, 15, 200, 25);
		firstNameFieldTitle.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField employeeFirstName = new JTextField();
		employeeFirstName.setBounds(190, 40, 150, 25);
		employeeFirstName.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JButton confirm = new JButton();
		confirm.setText("Ajouter");
		confirm.setBounds(75, 80, 80, 40);
		
		JButton cancel = new JButton();
		cancel.setText("Annuler");
		cancel.setBounds(220, 80, 80, 40);
		cancel.addActionListener(AppController.closeFrame(this));
		
		setTitle("Suppression d'employé");
		setSize(375, 175);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setVisible(true);
		
		panel.setLayout(null);
		panel.setVisible(true);
		panel.add(lastNameFieldTitle);
		panel.add(employeeLastName);
		panel.add(firstNameFieldTitle);
		panel.add(employeeFirstName);
		panel.add(confirm);
		panel.add(cancel);
		
		setContentPane(panel);
	}
}
