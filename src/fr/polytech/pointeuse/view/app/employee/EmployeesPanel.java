package fr.polytech.pointeuse.view.app.employee;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;

public class EmployeesPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public EmployeesPanel(Company comp) {
		
		JLabel employees = new JLabel("Employés de l'entreprise " + comp.getcName());
		employees.setFont(new Font("Arial", Font.BOLD, 16));
		employees.setBounds(50, 70, 300, 25);
		
		JTable employeesTable = new JTable(new EmployeesList(comp));
		
		JScrollPane table = new JScrollPane(employeesTable);
		table.setBounds(25, 150, 945, 530);
		
		JButton addEmployeeButton = new JButton();
		addEmployeeButton.setText("Ajouter employé");
		addEmployeeButton.setBounds(620, 100, 160, 30);
		addEmployeeButton.addActionListener(AppController.addEmployeeListener(comp));
		
		JButton removeEmployeeButton = new JButton();
		removeEmployeeButton.setText("Supprimer employé");
		removeEmployeeButton.setBounds(800, 100, 170, 30);
		removeEmployeeButton.addActionListener(AppController.removeEmployeeListener(comp));
		
		JLabel clock = new JLabel();
		clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
		clock.setFont(new Font("Arial", Font.BOLD, 14));
		clock.setBounds(825, 700, 150, 25);
		
		Timer timer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
            }
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
		
		
		setBackground(Color.WHITE);
		setLayout(null);
		add(employees);
		add(table);
		add(addEmployeeButton);
		add(removeEmployeeButton);
		add(clock);
	}
}

