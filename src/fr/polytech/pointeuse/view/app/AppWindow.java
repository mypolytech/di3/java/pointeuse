package fr.polytech.pointeuse.view.app;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.*;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.employee.EmployeesPanel;
import fr.polytech.pointeuse.view.app.history.HistoryPanel;
import fr.polytech.pointeuse.view.app.service.ServicesPanel;

public class AppWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	public AppWindow (String str) {
		
		setTitle(str + " App");
		setSize(1000, 800);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);
	}
	
	public AppWindow (Company comp) {
		
		JTabbedPane tabs = new JTabbedPane();
		HomePanel homeTab = new HomePanel(comp);
		ServicesPanel servicesTab = new ServicesPanel(comp);
		EmployeesPanel employeesTab = new EmployeesPanel(comp);
		HistoryPanel historyTab = new HistoryPanel(comp);
		
		this.setTitle(comp.getcName() + " - App");
		this.setSize(1000, 800);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		tabs.setBounds(0, 0, 0, 0);
		tabs.setFont(new Font("Arial", Font.BOLD, 15));
		
		tabs.add("Accueil", homeTab);
		tabs.add("Départements", servicesTab);
		tabs.add("Employés", employeesTab);
		tabs.add("Historique", historyTab);
		
		add(tabs);
		
		this.setVisible(true);
	}
	
}
