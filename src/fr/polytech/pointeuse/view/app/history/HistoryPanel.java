package fr.polytech.pointeuse.view.app.history;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;

public class HistoryPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static ArrayList<JTable> clockTables = new ArrayList<>();
	private static ArrayList<JScrollPane> tables = new ArrayList<>();
	private static JComboBox<String> employeesChoice = new JComboBox<>();

	public HistoryPanel(Company comp) {
		
		ArrayList<Employee> employeesList = new ArrayList<>();
		ArrayList<String> employeesNameList = new ArrayList<>();
		
		for (Service service : comp.getServices()) {
			for (Employee emp : service.getEmployees()) {
				employeesNameList.add(emp.getLastName() + " " + emp.getFirstName());
				employeesList.add(emp);
			}
		}
		
		Collections.sort(employeesNameList);
		
		employeesChoice.addItem("Tous");
		
		for (String str : employeesNameList) {
			employeesChoice.addItem(str);
		}
		
		employeesChoice.setBounds(50, 100, 200, 25);
		employeesChoice.addActionListener(AppController.changeClockTable(employeesChoice));
		
		JLabel employeesChoiceTitle = new JLabel("Choix de l'employé");
		employeesChoiceTitle.setFont(new Font("Arial", Font.BOLD, 16));
		employeesChoiceTitle.setBounds(50, 70, 200, 25);
		
		JButton addClockButton = new JButton();
		addClockButton.setText("Ajouter pointage");
		addClockButton.setBounds(620, 100, 160, 30);
		addClockButton.addActionListener(AppController.addClockListener(comp));
		
		JButton editClockButton = new JButton();
		editClockButton.setText("Modifier pointage");
		editClockButton.setBounds(800, 100, 170, 30);
		editClockButton.addActionListener(AppController.modifyClockListener(comp));
		
		clockTables.add(new JTable(new HistoryTable(comp)));
		clockTables.get(0).setName("Tous");
		
		for (Employee emp : employeesList) {
			clockTables.add(new JTable(new HistoryTable(emp)));
			clockTables.get(employeesList.indexOf(emp)+1).setName(employeesNameList.get(employeesList.indexOf(emp)));
		}
		
		for (JTable table : clockTables) {
			tables.add(new JScrollPane(table));
			tables.get(clockTables.indexOf(table)).setName(clockTables.get(clockTables.indexOf(table)).getName());
		}
			
		JLabel clock = new JLabel();
		clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
		clock.setFont(new Font("Arial", Font.BOLD, 14));
		clock.setBounds(825, 700, 150, 25);
		
		Timer timer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
            }
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
		
		setBackground(Color.WHITE);
		setLayout(null);
		add(employeesChoice);
		add(employeesChoiceTitle);
		add(addClockButton);
		add(editClockButton);

		for (JScrollPane table : tables) {
			add(table);
			table.setBounds(25, 150, 945, 530);
			table.setVisible(false);
		}
		tables.get(0).setVisible(true);
		add(clock);
	}
	
	public static void updateTable(String str) {
		
		for (JScrollPane table : tables) {
			if (table.getName().equals(str)) {
				table.setVisible(true);
			} else {
				table.setVisible(false);
			}
		}
	}
}

