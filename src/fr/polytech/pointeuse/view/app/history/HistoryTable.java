package fr.polytech.pointeuse.view.app.history;


import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.model.time.Clocked;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;

class HistoryTable extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private final ArrayList<Clocked> clockList;
    private final String[] tableHeaders = {"Date", "Heure d'arrivée", "Heure de départ", "Retard", "Avance", "Département"};

    public HistoryTable(Company company) {

        super();

        clockList = new ArrayList<>();

        for (Service service : company.getServices()) {
            for (Employee emp : service.getEmployees()) {
                clockList.addAll(emp.getWorkingDays());
            }
        }

        Collections.sort(clockList);
    }

    public HistoryTable(Employee emp) {

        super();
        clockList = new ArrayList<>();

        clockList.addAll(emp.getWorkingDays());

        Collections.sort(clockList);
    }

    public int getRowCount() {
        return clockList.size();
    }

    public int getColumnCount() {
        return tableHeaders.length;
    }

    public String getColumnName(int columnIndex) {
        return tableHeaders[columnIndex];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        switch(columnIndex) {

            case 0 :
                return clockList.get(rowIndex).getDateTimeClockIn().getRoundedDate().toString();

            case 1 :
                return clockList.get(rowIndex).getDateTimeClockIn().toString();

            case 2 :
                return clockList.get(rowIndex).getDateTimeClockOut().toString();

            case 3 :

            case 4 :
                return "TODO";

            default :
                return null;
        }
    }
}