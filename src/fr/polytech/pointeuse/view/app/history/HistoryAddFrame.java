package fr.polytech.pointeuse.view.app.history;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;

public class HistoryAddFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public HistoryAddFrame(Company comp) {
		
		JPanel panel = new JPanel();
		
		ArrayList<String> employeesNames = new ArrayList<String>();
		JComboBox<String> employeeChoice = new JComboBox<String>();
		
		for (Service serv : comp.getServices()) {
			for (Employee emp : serv.getEmployees()) {
				employeesNames.add(emp.getLastName() + " " + emp.getFirstName());
			}
		}
		
		Collections.sort(employeesNames);
		
		for (String str : employeesNames) {
			employeeChoice.addItem(str);
		}
		
		employeeChoice.setBounds(25, 20, 200, 25);
		
		JLabel date = new JLabel();
		date.setText("Date");
		date.setBounds(25, 65, 100, 25);
		date.setFont((new Font("Arial", Font.BOLD, 14)));

		JTextField dateValue = new JTextField();
		dateValue.setText("Format JJ/MM/AAAA");
		dateValue.setBounds(75, 65, 125, 25);
		dateValue.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel clockIn = new JLabel();
		clockIn.setText("Arrivée");
		clockIn.setBounds(25, 100, 200, 25);
		clockIn.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField clockInHours = new JTextField();
		clockInHours.setBounds(25, 125, 50, 25);
		clockInHours.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel inH = new JLabel();
		inH.setText("h");
		inH.setFont(new Font("Arial", Font.BOLD, 14));
		inH.setBounds(82, 127, 10, 25);
		
		JTextField clockInMinutes = new JTextField();
		clockInMinutes.setBounds(100, 125, 50, 25);
		clockInMinutes.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel clockOut = new JLabel();
		clockOut.setText("Départ");
		clockOut.setBounds(185, 100, 200, 25);
		clockOut.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField clockOutHours = new JTextField();
		clockOutHours.setBounds(185, 125, 50, 25);
		clockOutHours.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel outH = new JLabel();
		outH.setText("h");
		outH.setFont(new Font("Arial", Font.BOLD, 14));
		outH.setBounds(242, 127, 10, 25);
		
		JTextField clockOutMinutes = new JTextField();
		clockOutMinutes.setBounds(260, 125, 50, 25);
		clockOutMinutes.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JButton confirm = new JButton();
		confirm.setText("Ajouter");
		confirm.setBounds(75, 175, 80, 30);
		
		JButton cancel = new JButton();
		cancel.setText("Annuler");
		cancel.setBounds(225, 175, 80, 30);
		cancel.addActionListener(AppController.closeFrame(this));
		
		setTitle("Ajout de pointage");
		setSize(375, 255);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setVisible(true);
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.add(employeeChoice);
		panel.add(date);
		panel.add(dateValue);
		panel.add(clockIn);
		panel.add(clockInHours);
		panel.add(inH);
		panel.add(clockInMinutes);
		panel.add(clockOut);
		panel.add(clockOutHours);
		panel.add(outH);
		panel.add(clockOutMinutes);
		panel.add(confirm);
		panel.add(cancel);
		
		setContentPane(panel);
	}
}
