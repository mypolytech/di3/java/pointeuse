package fr.polytech.pointeuse.view.app.history;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.polytech.pointeuse.model.company.Company;

public class HistoryModifyFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public HistoryModifyFrame(Company comp) {
		
		JPanel panel = new JPanel();
		
		JLabel todo = new JLabel("TODO");
		todo.setBounds(25, 25, 100, 25);
		todo.setFont(new Font("Arial", Font.BOLD, 14));
		
		setTitle("Modification de pointage");
		setSize(375, 255);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setVisible(true);
		
		panel.setLayout(null);
		panel.setVisible(true);
		panel.add(todo);
		
		setContentPane(panel);
	}
}
