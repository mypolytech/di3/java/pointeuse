package fr.polytech.pointeuse.view.app.service;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Manager;
import fr.polytech.pointeuse.model.company.Service;

public class ServiceChangeManager extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public ServiceChangeManager(Company comp) {
		
		JPanel panel = new JPanel();
		
		ArrayList<Manager> managers = new ArrayList<Manager>();
			
		JComboBox<String> serviceChoice = new JComboBox<String>();
		JComboBox<String> managerChoice = new JComboBox<String>();
		
		for (Service serv : comp.getServices()) {
			serviceChoice.addItem(serv.getServiceName());
			for (Employee emp : serv.getEmployees()) {
				if (emp.getClass() == Manager.class) {
					managers.add((Manager) emp);
				}
			}
		}
		
		serviceChoice.setBounds(20,	50, 200, 25);
		
		for (Manager manager : managers) {
			
			if (!manager.isManaging()) {
				managerChoice.addItem(manager.getLastName() + " " + manager.getFirstName());
			}
		}
		
		managerChoice.setBounds(20, 80, 200, 25);
		
		JLabel title = new JLabel();
		title.setText("Choix du service et du manager");
		title.setFont(new Font("Arial", Font.BOLD, 14));
		title.setBounds(20, 20, 250, 25);
 		
		JButton modify = new JButton();
		modify.setText("Changer");
		modify.setBounds(40, 125, 100, 30);
		modify.addActionListener(AppController.changeManagerConfirm(comp, serviceChoice, managerChoice, this));
		
		JButton cancel = new JButton();
		cancel.setText("Annuler");
		cancel.setBounds(160, 125, 100, 30);
		cancel.addActionListener(AppController.closeFrame(this));
		
		setTitle("Manager");
		setSize(300, 200);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setVisible(true);
		
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.add(serviceChoice);
		panel.add(managerChoice);
		panel.add(title);
		panel.add(modify);
		panel.add(cancel);
		
		setContentPane(panel);
	}
}
