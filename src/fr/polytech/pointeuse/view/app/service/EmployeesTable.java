package fr.polytech.pointeuse.view.app.service;


import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;

class EmployeesTable extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private final ArrayList<Employee> employeesList;
    private final String[] tableHeaders = {"ID", "Nom", "Prénom", "Début", "Fin", "Balance", "Statut"};

    public EmployeesTable(Service serv) {

        super();

        employeesList = new ArrayList<Employee>();

        for (Employee emp : serv.getEmployees()) {
            employeesList.add(emp);
        }

        Collections.sort(employeesList);
    }

    public int getRowCount() {
        return employeesList.size();
    }

    public int getColumnCount() {
        return tableHeaders.length;
    }

    public String getColumnName(int columnIndex) {
        return tableHeaders[columnIndex];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        switch(columnIndex) {

            case 0 :
                return employeesList.get(rowIndex).getId();

            case 1 :
                return employeesList.get(rowIndex).getLastName();

            case 2 :
                return employeesList.get(rowIndex).getFirstName();

            case 3 :
                return employeesList.get(rowIndex).getTimeIn().toString();

            case 4 :
                return employeesList.get(rowIndex).getTimeOut().toString();

            case 5 :
                return employeesList.get(rowIndex).getTimeBalance();

            case 6 :
                return employeesList.get(rowIndex).getClass().getSimpleName();

            default :
                return null;
        }
    }
}