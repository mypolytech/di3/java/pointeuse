package fr.polytech.pointeuse.view.app.service;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;

public class DeleteConfirmationBox extends JDialog {

	private static final long serialVersionUID = 1L;
	
	public DeleteConfirmationBox(Company comp, JComboBox<String> serviceChoice) {
		
		JPanel panel = new JPanel();
		
		JLabel msg = new JLabel();
		msg.setText("La suppression du département supprime tous les employés associés");
		msg.setBounds(25, 20, 491, 25);
		msg.setFont(new Font("Arial", Font.BOLD, 14));
		
		JLabel msg2 = new JLabel();
		msg2.setText("Voulez-vous continuer ?");
		msg2.setBounds(185, 50, 172, 25);
		msg2.setFont(new Font("Arial", Font.BOLD, 14));
		
		JButton yes = new JButton();
		yes.setText("Oui");
		yes.setBounds(190, 86, 60, 30);
		yes.addActionListener(AppController.removingService(comp, serviceChoice, this));
		
		JButton no = new JButton();
		no.setText("Non");
		no.setBounds(287, 86, 60, 30);
		no.addActionListener(AppController.closeDialog(this));
		
		
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setVisible(true);
		panel.add(msg);
		panel.add(msg2);
		panel.add(yes);
		panel.add(no);
		
		setTitle("Confirmation");
		setVisible(false);
		setResizable(false);
		setSize(550, 160);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setContentPane(panel);
	}
}
