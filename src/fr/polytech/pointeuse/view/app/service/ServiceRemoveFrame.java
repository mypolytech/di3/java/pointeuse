package fr.polytech.pointeuse.view.app.service;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Service;

public class ServiceRemoveFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public ServiceRemoveFrame(Company comp) {
		
		ArrayList<String> serviceNames = new ArrayList<String>();
		
		for(Service serv : comp.getServices()) {
			serviceNames.add(serv.getServiceName());
		}
		
		Collections.sort(serviceNames);
		
		JPanel panel = new JPanel();
		
		JLabel serviceChoiceTitle = new JLabel();
		serviceChoiceTitle.setText("Choix du service à supprimer");
		serviceChoiceTitle.setBounds(20, 20, 300, 25);
		serviceChoiceTitle.setFont(new Font("Arial", Font.BOLD, 14));
		
		JComboBox<String> servicesChoice = new JComboBox<String>();
		
		for(String str : serviceNames) {
			servicesChoice.addItem(str);
		}
		
		servicesChoice.setBounds(50, 50, 150, 25);
		
		JButton delete = new JButton();
		delete.setText("Supprimer");
		delete.setBounds(10, 86, 100, 30);
		delete.addActionListener(AppController.removeServiceConfirm(comp, servicesChoice, delete));
		
		JButton cancel = new JButton();
		cancel.setText("Annuler");
		cancel.setBounds(139, 86, 100, 30);
		cancel.addActionListener(AppController.closeFrame(this));
		
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.add(serviceChoiceTitle);
		panel.add(servicesChoice);
		panel.add(delete);
		panel.add(cancel);
		
		setTitle("Supprimer département");
		setVisible(true);
		setResizable(false);
		setSize(255, 160);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setContentPane(panel);
	}
}
