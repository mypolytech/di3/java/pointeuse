package fr.polytech.pointeuse.view.app.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Service;

public class ServicesPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static JComboBox<String> servicesChoice = new JComboBox<String>();
	private static ArrayList<JTable> servicesTables = new ArrayList<JTable>();
	private static ArrayList<JScrollPane> tables = new ArrayList<JScrollPane>();

	public ServicesPanel(Company comp) {
		
		ArrayList<Service> services = new ArrayList<Service>();
		ArrayList<String> servicesNames = new ArrayList<String>();
		
		// Service selection		
		for(Service serv : comp.getServices()) {
			services.add(serv);
			servicesNames.add(serv.getServiceName());
		}
		
		Collections.sort(servicesNames);
		
		for(String str : servicesNames) {
			servicesChoice.addItem(str);			
		}
		
		servicesChoice.setBounds(50, 100, 200, 25);
		
		servicesChoice.addActionListener(AppController.changeServiceTable(servicesChoice));
		
		// Combo box's title
		JLabel servicesChoiceTitle = new JLabel("Choix du département");
		servicesChoiceTitle.setFont(new Font("Arial", Font.BOLD, 16));
		servicesChoiceTitle.setBounds(50, 70, 200, 25);
		
		for (Service serv : services) {
			servicesTables.add(new JTable(new EmployeesTable(serv)));
			servicesTables.get(services.indexOf(serv)).setName(serv.getServiceName());
		}
		
		for (JTable table : servicesTables) {
			tables.add(new JScrollPane(table));
			tables.get(servicesTables.indexOf(table)).setName(servicesTables.get(servicesTables.indexOf(table)).getName());
		}
		
		JLabel clock = new JLabel();
		clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
		clock.setFont(new Font("Arial", Font.BOLD, 14));
		clock.setBounds(825, 700, 150, 25);
		
		JButton addServiceButton = new JButton();
		addServiceButton.setText("Ajouter département");
		addServiceButton.setBounds(620, 100, 160, 30);
		addServiceButton.addActionListener(AppController.addServiceListener(comp));
		
		JButton removeServiceButton = new JButton();
		removeServiceButton.setText("Supprimer département");
		removeServiceButton.setBounds(800, 100, 170, 30);
		removeServiceButton.addActionListener(AppController.removeServiceListener(comp));
		
		JButton changeManager = new JButton();
		changeManager.setText("Changer un manager");
		changeManager.setBounds(700, 50, 200, 30);
		changeManager.addActionListener(AppController.changeServiceManager(comp));
		
		Timer timer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
            }
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
		
		setBackground(Color.WHITE);
		setLayout(null);
		add(servicesChoice);
		add(servicesChoiceTitle);
		
		for (JScrollPane table : tables) {
			add(table);
			table.setBounds(25, 150, 945, 530);
			table.setVisible(false);
		}

		tables.get(0).setVisible(true);
		
		add(addServiceButton);
		add(removeServiceButton);
		add(changeManager);
		add(clock);
	}
	
	public static void updateServiceChoice(Company comp) {
		
		servicesChoice.removeAllItems();
		
		ArrayList<String> servicesNames = new ArrayList<String>();
		
		for(Service serv : comp.getServices()) {
			servicesNames.add(serv.getServiceName());
		}
		
		Collections.sort(servicesNames);
		
		for(String str : servicesNames) {
			servicesChoice.addItem(str);			
		}
	}
	
	public static void updateTable(String str) {
		
		for (JScrollPane table : tables) {
			if (table.getName() == str) {
				table.setVisible(true);
			} else {
				table.setVisible(false);
			}
		}
	}
}

