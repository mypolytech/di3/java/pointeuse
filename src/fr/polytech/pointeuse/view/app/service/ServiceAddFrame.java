package fr.polytech.pointeuse.view.app.service;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.polytech.pointeuse.controller.app.AppController;
import fr.polytech.pointeuse.model.company.Company;

public class ServiceAddFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public ServiceAddFrame(Company comp) {
		
		JPanel panel = new JPanel();
		
		JLabel nameFieldTitle = new JLabel();
		nameFieldTitle.setText("Nom du département");
		nameFieldTitle.setBounds(25, 15, 200, 25);
		nameFieldTitle.setFont(new Font("Arial", Font.BOLD, 14));
		
		JTextField serviceNameField = new JTextField();
		serviceNameField.setBounds(25, 40, 200, 25);
		serviceNameField.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JButton addServiceButton = new JButton();
		addServiceButton.setText("Ajouter");
		addServiceButton.setBounds(25, 85, 80, 25);
		addServiceButton.addActionListener(AppController.addServiceConfirm(comp, addServiceButton, serviceNameField, this));
		
		JButton cancel = new JButton();
		cancel.setText("Annuler");
		cancel.setBounds(145, 85, 80, 25);
		cancel.addActionListener(AppController.closeFrame(this));
		
		setTitle("Ajout de département");
		setSize(255, 160);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setResizable(false);
		setVisible(true);
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.add(nameFieldTitle);
		panel.add(serviceNameField);
		panel.add(addServiceButton);
		panel.add(cancel);
		
		setContentPane(panel);
	}
}
