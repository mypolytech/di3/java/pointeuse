package fr.polytech.pointeuse.view.app;

import javax.swing.JPanel;
import javax.swing.Timer;

import fr.polytech.pointeuse.model.company.Company;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;

public class HomePanel extends JPanel {

	// ===== Attributes =====
	
	private static final long serialVersionUID = 1L;
	private JLabel companyName = new JLabel();
	private Icon logo = new ImageIcon("./Fictice.png");
	private JLabel logoDisplay = new JLabel();
	private JLabel companyStats = new JLabel();
	private JLabel workforce = new JLabel();
	private JLabel services = new JLabel();
	private JLabel boss = new JLabel();
	
	// ===== Constructors =====
	
	public HomePanel(Company comp) {
		
		// Get the company name and displays it
		companyName.setText("Entreprise : " + comp.getcName());
		companyName.setBounds(50, 50, 200, 25);
		companyName.setFont(new Font("Arial", Font.BOLD, 22));
		
		// Get the company's logo and displays it
		logoDisplay.setBounds(650, 50, 295, 144);
		logoDisplay.setIcon(logo);
		logoDisplay.setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Get company's stats and display them
		companyStats.setText("Statistiques de l'entreprise :");
		companyStats.setFont(new Font("Arial", Font.BOLD, 18));
		companyStats.setBounds(50, 122, 255, 25);
		
		workforce.setText("- Nombre d'employés : " + comp.getTotalWorkforce());
		workforce.setFont(new Font("Arial", Font.BOLD, 16));
		workforce.setBounds(75, 155, 255, 25);
		
		services.setText("- Nombre de services : " + comp.getTotalServicesNumber());
		services.setFont(new Font("Arial", Font.BOLD, 16));
		services.setBounds(75, 180, 255, 25);
		
		//boss.setText("Patron : " + comp.getBoss().getFirstName() + " " + comp.getBoss().getLastName());
		boss.setFont(new Font("Arial", Font.BOLD, 18));
		boss.setBounds(50, 250, 255, 25);
		
		// Get the current time and displays it
		JLabel clock = new JLabel();
		clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
		clock.setFont(new Font("Arial", Font.BOLD, 14));
		clock.setBounds(825, 700, 150, 25);
		
		Timer timer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	clock.setText(DateFormat.getDateTimeInstance().format(new Date()));
            }
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
		
        // Panel setup
        setLayout(null);
		setBackground(Color.WHITE);
		add(companyName);
		add(logoDisplay);
		add(companyStats);
		add(workforce);
		add(services);
		add(boss);
		add(clock);
	}
}
