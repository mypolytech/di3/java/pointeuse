package fr.polytech.pointeuse.dao;

import fr.polytech.pointeuse.model.company.Employee;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.List;

public class EmployeeDAO extends BaseDao<Employee> {
	
	private static final Logger LOGGER = LogManager.getLogger(EmployeeDAO.class);

	public List<Employee> getAllEmployees() {
		Session session = sessionFactory.openSession();
		String hql = "FROM Employee";
		Query query = session.createQuery(hql);
		List<Employee> employees = query.list();
		session.close();
		return employees;
	}

	public Employee getEmployeeById(int id) {
		Employee employee = null;
		Session session = sessionFactory.openSession();
		Query query = session.createSQLQuery("SELECT * FROM Employee WHERE Employee.id = :id");
		query.setParameter("id", id);
		try {
			employee = (Employee) query.getSingleResult();
		} catch (NoResultException noResult) {
			LOGGER.error(noResult.getMessage());
		}

		return employee;
	}

}
