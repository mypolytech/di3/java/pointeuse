package fr.polytech.pointeuse.controller.emulator;

import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.model.tcp.EmulatorClient;
import fr.polytech.pointeuse.view.emulator.EmulatorWindow;

import java.util.ArrayList;

class Sync implements Runnable {

    private EmulatorClient emuClient;
    private ArrayList<SimpleEmployee> empList;

    public Sync(EmulatorClient emuClient) {
        this.emuClient = emuClient;
        this.empList = emuClient.getPacket().getEmployeesList();
    }

    @Override
    public void run() {

        // First sync, let it time to get infos
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        System.out.println("Updating emulator...");
        empList = emuClient.getPacket().getEmployeesList();

        EmulatorWindow.updateEmployeesList(empList);
    }
}
