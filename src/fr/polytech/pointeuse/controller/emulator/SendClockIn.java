package fr.polytech.pointeuse.controller.emulator;

import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.model.tcp.AppClient;
import fr.polytech.pointeuse.model.tcp.EmulatorPacket;
import fr.polytech.pointeuse.model.time.RoundedDateTime;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class SendClockIn implements ActionListener {

    private JComboBox<String> employeesList;
    private ArrayList<SimpleEmployee> empList;
    private Integer ID;
    private RoundedDateTime roundedDateTime;

    public SendClockIn(JComboBox<String> employeesList, ArrayList<SimpleEmployee> empList, RoundedDateTime roundedDateTime) {
        this.employeesList = employeesList;
        this.empList = empList;
        this.roundedDateTime = roundedDateTime;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        for (SimpleEmployee simEmp : empList) {
            if (simEmp.getEmployeeName() == employeesList.getSelectedItem()) {
                ID = simEmp.getID();
            }
        }

        AppClient appClient = new AppClient(8086);
        EmulatorPacket emuPacket = new EmulatorPacket(ID, roundedDateTime, "in");
        appClient.setPacket(emuPacket);

        Thread sendingThread = new Thread(appClient);
        sendingThread.start();
    }
}
