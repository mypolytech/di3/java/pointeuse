package fr.polytech.pointeuse.controller.emulator;

import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.model.tcp.EmulatorClient;
import fr.polytech.pointeuse.model.time.RoundedDateTime;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;

public abstract class EmulatorController {

    public static Thread createEmulatorSync(EmulatorClient emuClient) {
        Sync sync = new Sync(emuClient);
        return new Thread(sync);
    }

    public static ActionListener emulatorInfosUpdate(ArrayList<SimpleEmployee> employeesList, JComboBox<String> employeesChoice) {
        return new InfosUpdate(employeesList, employeesChoice);
    }

    public static ActionListener emulatorSendClockIn(JComboBox<String> employeesList, ArrayList<SimpleEmployee> empList, LocalDate date, RoundedDateTime roundedDateTime) {
        return new SendClockIn(employeesList, empList, roundedDateTime);
    }

    public static ActionListener emulatorSendClockOut(JComboBox<String> employeesList, ArrayList<SimpleEmployee> empList, LocalDate date, RoundedDateTime roundedDateTime) {
        return new SendClockOut(employeesList, empList, roundedDateTime);
    }
}
