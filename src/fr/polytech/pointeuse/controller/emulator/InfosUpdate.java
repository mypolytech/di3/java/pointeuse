package fr.polytech.pointeuse.controller.emulator;

import fr.polytech.pointeuse.model.company.SimpleEmployee;
import fr.polytech.pointeuse.view.emulator.EmulatorWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class InfosUpdate implements ActionListener {

    private ArrayList<SimpleEmployee> employeesList;
    private JComboBox<String> employeesChoice;

    public InfosUpdate(ArrayList<SimpleEmployee> employeesList, JComboBox<String> employeesChoice) {
        this.employeesList = employeesList;
        this.employeesChoice = employeesChoice;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == employeesChoice) {
            for (SimpleEmployee simEmp : employeesList) {
                if (employeesChoice.getSelectedItem() == simEmp.getEmployeeName()) {
                    EmulatorWindow.updateInfosDisplayed(simEmp.getID(), simEmp.getEmployeeName(), simEmp.getEmployeeService());
                }
            }
        }
    }
}
