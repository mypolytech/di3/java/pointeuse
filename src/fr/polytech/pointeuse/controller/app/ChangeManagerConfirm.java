package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Manager;
import fr.polytech.pointeuse.model.company.Service;

class ChangeManagerConfirm implements ActionListener {

	private Company comp;
	private JComboBox<String> serviceChoice;
	private JComboBox<String> managerChoice;
	private JFrame frame;

	public ChangeManagerConfirm(Company comp, JComboBox<String> serviceChoice, JComboBox<String> managerChoice,
			JFrame frame) {
		this.comp = comp;
		this.serviceChoice = serviceChoice;
		this.managerChoice = managerChoice;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String serv = serviceChoice.getSelectedItem().toString();
		String manager = managerChoice.getSelectedItem().toString();

		String lastName = manager.substring(0, manager.indexOf(" "));
		String firstName = manager.substring(manager.indexOf(" ") + 1, manager.length());

		for (Service service : comp.getServices()) {
			if (service.getServiceName() == serv) {
				for (Employee emp : service.getEmployees()) {
					if (emp.getLastName() == lastName && emp.getFirstName() == firstName) {
						try {
							service.changeManager((Manager) emp);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		}

		frame.dispose();
	}
}