package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.service.ServiceAddFrame;

class AddServiceButtonListener implements ActionListener {

	private Company comp;

	public AddServiceButtonListener(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		ServiceAddFrame serviceAddFrame = new ServiceAddFrame(comp);
	}
}
