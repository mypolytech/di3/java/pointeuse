package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.employee.EmployeeAddFrame;

class AddEmployeeButtonListener implements ActionListener {

	private Company comp;

	public AddEmployeeButtonListener(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		EmployeeAddFrame employeeAddFrame = new EmployeeAddFrame(comp);
	}
}
