package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.history.HistoryAddFrame;

class AddClockButtonListener implements ActionListener {

	private Company comp;

	public AddClockButtonListener(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		HistoryAddFrame historyAddFrame = new HistoryAddFrame(comp);
	}
}