package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.service.ServiceRemoveFrame;

class RemoveServiceButtonListener implements ActionListener {

	private Company comp;

	public RemoveServiceButtonListener(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		ServiceRemoveFrame serviceRemoveFrame = new ServiceRemoveFrame(comp);
	}
}