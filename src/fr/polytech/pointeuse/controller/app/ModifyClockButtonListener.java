package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.history.HistoryModifyFrame;

class ModifyClockButtonListener implements ActionListener {

	private Company comp;

	public ModifyClockButtonListener(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		HistoryModifyFrame historyModifyFrame = new HistoryModifyFrame(comp);
	}
}