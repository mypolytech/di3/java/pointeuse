package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import fr.polytech.pointeuse.view.app.history.HistoryPanel;

class ChangeClockTable implements ActionListener {

	private JComboBox<String> employeeChoice;

	public ChangeClockTable(JComboBox<String> employeeChoice) {
		this.employeeChoice = employeeChoice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == employeeChoice) {
			String str = (String) employeeChoice.getSelectedItem();
			HistoryPanel.updateTable(str);
		}
	}
}