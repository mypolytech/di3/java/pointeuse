package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.view.app.AppErrorBox;
import fr.polytech.pointeuse.view.app.service.ServicesPanel;

class AddServiceConfirm implements ActionListener {

	private Company comp;
	private JTextField textField;
	private JButton button;
	private AppErrorBox invalidName = new AppErrorBox("Nom de service invalide, veuillez ressaisir.");;
	private JFrame frame;

	public AddServiceConfirm(Company comp, JButton button, JTextField textField, JFrame frame) {
		this.comp = comp;
		this.textField = textField;
		this.button = button;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {

			String str = textField.getText();
			if (str.length() == 0) {
				invalidName.setVisible(true);
			} else if (!str.matches("[a-zA-Z ]*")) {
				invalidName.setVisible(true);
			} else {

				String temp = str.substring(0, 1);
				temp = temp.toUpperCase();

				temp += str.substring(1);

				try {
					comp.addService(new Service(temp));
				} catch (Exception error) {
					error.getMessage();
				}

				System.out.println("Service " + str + " ajouté avec succès.");
				for (Service serv : comp.getServices()) {
					System.out.println(serv.getServiceName());
				}
				frame.dispose();
				ServicesPanel.updateServiceChoice(comp);
			}
		}
	}
}