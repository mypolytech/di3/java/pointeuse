package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.service.DeleteConfirmationBox;

class RemoveServiceConfirm implements ActionListener {

	private JButton button;
	private DeleteConfirmationBox confirm;

	public RemoveServiceConfirm(Company comp, JComboBox<String> serviceChoice, JButton button) {
		this.button = button;
		this.confirm = new DeleteConfirmationBox(comp, serviceChoice);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == button) {

			confirm.setVisible(true);
		}
	}
}