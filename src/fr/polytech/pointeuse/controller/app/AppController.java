package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;

import fr.polytech.pointeuse.model.company.Company;

public abstract class AppController {

	// ===== Action listeners =====

	public static ActionListener addServiceListener(Company comp) {
		AddServiceButtonListener addServiceListener = new AddServiceButtonListener(comp);
		return addServiceListener;
	}

	public static ActionListener addServiceConfirm(Company comp, JButton button, JTextField textField, JFrame frame) {
		AddServiceConfirm addServiceConfirm = new AddServiceConfirm(comp, button, textField, frame);
		return addServiceConfirm;
	}

	public static ActionListener removeServiceListener(Company comp) {
		RemoveServiceButtonListener removeServiceButtonListener = new RemoveServiceButtonListener(comp);
		return removeServiceButtonListener;
	}

	public static ActionListener removeServiceConfirm(Company comp, JComboBox<String> serviceChoice, JButton button) {
		RemoveServiceConfirm removeServiceConfirm = new RemoveServiceConfirm(comp, serviceChoice, button);
		return removeServiceConfirm;
	}

	public static ActionListener removingService(Company comp, JComboBox<String> serviceChoice, JDialog dialog) {
		RemovingService removingService = new RemovingService(comp, serviceChoice, dialog);
		return removingService;
	}

	public static ActionListener changeServiceManager(Company comp) {
		ChangeServiceManager changeServiceManager = new ChangeServiceManager(comp);
		return changeServiceManager;
	}

	public static ActionListener changeManagerConfirm(Company comp, JComboBox<String> serviceChoice,
			JComboBox<String> managerChoice, JFrame frame) {
		ChangeManagerConfirm changeManagerConfirm = new ChangeManagerConfirm(comp, serviceChoice, managerChoice, frame);
		return changeManagerConfirm;
	}

	public static ActionListener changeServiceTable(JComboBox<String> serviceChoice) {
		ChangeServiceTable changeServiceTable = new ChangeServiceTable(serviceChoice);
		return changeServiceTable;
	}

	public static ActionListener addEmployeeListener(Company comp) {
		AddEmployeeButtonListener addEmployeeListener = new AddEmployeeButtonListener(comp);
		return addEmployeeListener;
	}

	public static ActionListener removeEmployeeListener(Company comp) {
		RemoveEmployeeButtonListener removeEmployeeListener = new RemoveEmployeeButtonListener(comp);
		return removeEmployeeListener;
	}

	public static ActionListener addClockListener(Company comp) {
		AddClockButtonListener addClockButtonListener = new AddClockButtonListener(comp);
		return addClockButtonListener;
	}

	public static ActionListener changeClockTable(JComboBox<String> employeeChoice) {
		ChangeClockTable changeClockTable = new ChangeClockTable(employeeChoice);
		return changeClockTable;
	}

	public static ActionListener modifyClockListener(Company comp) {
		ModifyClockButtonListener modifiyClockListener = new ModifyClockButtonListener(comp);
		return modifiyClockListener;
	}

	public static ActionListener closeFrame(JFrame frame) {
		CloseFrame close = new CloseFrame(frame);
		return close;
	}

	public static ActionListener closeDialog(JDialog dialog) {
		CloseFrame close = new CloseFrame(dialog);
		return close;
	}

}