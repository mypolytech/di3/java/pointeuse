package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.service.ServiceChangeManager;

class ChangeServiceManager implements ActionListener {

	private Company comp;

	public ChangeServiceManager(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		ServiceChangeManager serviceChangeManager = new ServiceChangeManager(comp);
	}
}