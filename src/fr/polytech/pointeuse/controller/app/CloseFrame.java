package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;

class CloseFrame implements ActionListener {

	private JFrame frame;
	private JDialog dialog;

	public CloseFrame(JFrame frame) {
		this.frame = frame;
	}

	public CloseFrame(JDialog dialog) {
		this.dialog = dialog;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (frame != null) {
			frame.dispose();
		}

		if (dialog != null) {
			dialog.dispose();
		}
	}
}