package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.view.app.employee.EmployeeRemoveFrame;

class RemoveEmployeeButtonListener implements ActionListener {

	private Company comp;

	public RemoveEmployeeButtonListener(Company comp) {
		this.comp = comp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		EmployeeRemoveFrame employeeRemoveFrame = new EmployeeRemoveFrame(comp);
	}
}