package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import fr.polytech.pointeuse.view.app.service.ServicesPanel;

class ChangeServiceTable implements ActionListener {

	private JComboBox<String> serviceChoice;

	public ChangeServiceTable(JComboBox<String> serviceChoice) {
		this.serviceChoice = serviceChoice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == serviceChoice) {
			String str = (String) serviceChoice.getSelectedItem();
			ServicesPanel.updateTable(str);
		}
	}
}