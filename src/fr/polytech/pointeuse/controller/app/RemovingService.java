package fr.polytech.pointeuse.controller.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JDialog;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.view.app.service.ServicesPanel;

class RemovingService implements ActionListener {

	private Company comp;
	private JComboBox<String> serviceChoice;
	private JDialog dialog;

	public RemovingService(Company comp, JComboBox<String> serviceChoice, JDialog dialog) {
		this.comp = comp;
		this.serviceChoice = serviceChoice;
		this.dialog = dialog;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		for (Service serv : comp.getServices()) {
			if (serv.getServiceName() == serviceChoice.getSelectedItem()) {
				comp.removeService(serv);
			}
		}

		ServicesPanel.updateServiceChoice(comp);
		dialog.dispose();
	}
}