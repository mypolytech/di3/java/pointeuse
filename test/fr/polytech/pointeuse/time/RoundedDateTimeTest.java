package fr.polytech.pointeuse.time;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import fr.polytech.pointeuse.model.time.RoundedDateTime;

public class RoundedDateTimeTest {
	
	// == Rounded time test ==
	@Test
	public void roundZero() {
		RoundedDateTime rdt = new RoundedDateTime(LocalDateTime.of(LocalDate.of(1999,12,31),LocalTime.of(1, 6)));
		assertTrue(rdt.getRoundedTime().getHour() == 1 && rdt.getRoundedTime().getMinute() == 0);
	}
	
	@Test
	public void roundFirstQuarter() {
		RoundedDateTime rdt = new RoundedDateTime(LocalDateTime.of(LocalDate.of(1999,12,31),LocalTime.of(1, 10)));
		assertTrue(rdt.getRoundedTime().getHour() == 1 && rdt.getRoundedTime().getMinute() == 15);
	}
	
	@Test
	public void roundHalf() {
		RoundedDateTime rdt = new RoundedDateTime(LocalDateTime.of(LocalDate.of(1999,12,31),LocalTime.of(1, 31)));
		assertTrue(rdt.getRoundedTime().getHour() == 1 && rdt.getRoundedTime().getMinute() == 30);
	}
	
	@Test
	public void roundThirdQuarter() {
		RoundedDateTime rdt = new RoundedDateTime(LocalDateTime.of(LocalDate.of(1999,12,31),LocalTime.of(1, 46)));
		assertTrue(rdt.getRoundedTime().getHour() == 1 && rdt.getRoundedTime().getMinute() == 45);
	}
	
	@Test
	public void roundHour() {
		RoundedDateTime rdt = new RoundedDateTime(LocalDateTime.of(LocalDate.of(1999,12,31),LocalTime.of(1, 57)));
		assertTrue(rdt.getRoundedTime().getHour() == 2 && rdt.getRoundedTime().getMinute() == 0);
	}
	// =======================
}
