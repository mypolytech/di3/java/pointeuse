package fr.polytech.pointeuse.model;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import fr.polytech.pointeuse.model.company.Boss;
import fr.polytech.pointeuse.model.company.BossException;
import fr.polytech.pointeuse.model.company.Company;

public class BossTest {

	// == Add company tests ==
	@Test
	public void addCompany() throws BossException {
		Boss boss = new Boss("A", "B");
		Company comp = new Company("A", "B");
		boss.addCompany(comp);
		assertTrue(boss.getCompanies().size() == 1 && boss.getCompanies().contains(comp));
	}

	@Test
	public void addSameCompany() throws BossException {
		Boss boss = new Boss("A", "B");
		Company comp = new Company("A", "B");
		boss.addCompany(comp);
		assertThrows(BossException.class, () -> boss.addCompany(comp));
	}
	// =======================
}
