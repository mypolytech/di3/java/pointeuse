package fr.polytech.pointeuse.model;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import org.apache.log4j.PropertyConfigurator;

import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Manager;
import fr.polytech.pointeuse.model.company.Service;
import fr.polytech.pointeuse.model.company.ServiceException;

public class ServiceTest {

	// == Setup ==
	@BeforeClass
	public static void init() {
		// In order to avoid log4j errors when executing tests
		PropertyConfigurator.configure("log4j.properties");
	}
	// ===========

	// == Add Employee Tests ==
	@Test
	public void addEmployee() throws ServiceException {
		Service serv = new Service("Test");
		Employee emp = new Employee("A", "B");
		serv.addEmployee(emp);
		assertTrue(serv.getEmployees().size() == 1 && serv.getEmployees().contains(emp));
	}

	@Test
	public void addMultipleEmployees() throws ServiceException {
		Service serv = new Service("Test");
		Employee emp1 = new Employee("A", "B");
		Employee emp2 = new Employee("C", "D");
		serv.addEmployee(emp1);
		serv.addEmployee(emp2);
		assertTrue(serv.getEmployees().size() == 2 && serv.getEmployees().contains(emp1)
				&& serv.getEmployees().contains(emp2));
	}

	@Test
	public void addSameEmployee() throws ServiceException {
		Service serv = new Service("Test");
		Employee emp = new Employee("A", "B");
		serv.addEmployee(emp);
		assertThrows(ServiceException.class, () -> serv.addEmployee(emp));
	}
	// ========================

	// == Add Manager Tests ==
	@Test
	public void addManager() throws ServiceException {
		Service serv = new Service("Test");
		Manager man = new Manager("A", "B");
		serv.addEmployee(man);
		assertTrue(serv.getEmployees().size() == 1 && serv.getEmployees().contains(man));
	}

	@Test
	public void addMultipleManagers() throws ServiceException {
		Service serv = new Service("Test");
		Manager man1 = new Manager("A", "B");
		Manager man2 = new Manager("C", "D");
		serv.addEmployee(man1);
		serv.addEmployee(man2);
		assertTrue(serv.getEmployees().size() == 2 && serv.getEmployees().contains(man1)
				&& serv.getEmployees().contains(man2));
	}

	@Test
	public void addSameManager() throws ServiceException {
		Service serv = new Service("Test");
		Manager man = new Manager("A", "B");
		serv.addEmployee(man);
		assertThrows(ServiceException.class, () -> serv.addEmployee(man));
	}
	// =======================

	// == Remove employee tests ==
	@Test
	public void removeEmployee() throws ServiceException {
		Service serv = new Service("Test");
		Employee emp = new Employee("A", "B");
		serv.addEmployee(emp);
		serv.removeEmployee(emp);
		assertTrue(serv.getEmployees().isEmpty() && !serv.getEmployees().contains(emp));
	}

	@Test
	public void removeManager() throws ServiceException {
		Service serv = new Service("Test");
		Manager man = new Manager("A", "B");
		serv.addEmployee(man);
		serv.removeEmployee(man);
		assertTrue(serv.getEmployees().isEmpty() && !serv.getEmployees().contains(man));
	}
	// ============================

	// == Change manager tests ==
	@Test
	public void changeManager() throws ServiceException {
		Service serv = new Service("Test");
		Manager man = new Manager("A", "B");
		serv.changeManager(man);
		assertTrue(serv.getManager().equals(man));
	}
	// ==========================

	// == Work force test ==
	@Test
	public void checkWorkforce() throws ServiceException {
		Service serv = new Service("Test");
		Employee emp = new Employee("A", "B");
		Manager man = new Manager("A", "B");
		serv.addEmployee(emp);
		serv.addEmployee(man);
		assertTrue(serv.getWorkforce() == 2);
	}
	// ======================
}
