package fr.polytech.pointeuse.model;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import fr.polytech.pointeuse.model.company.Company;
import fr.polytech.pointeuse.model.company.CompanyException;
import fr.polytech.pointeuse.model.company.Service;

public class CompanyTest {

	// == Add service tests ==
	@Test
	public void addService() throws CompanyException {
		Company comp = new Company("Test", "Addresse");
		Service serv = new Service("Service");
		comp.addService(serv);
		assertTrue(comp.getServices().size() == 1 && comp.getServices().contains(serv));
	}

	@Test
	public void addMultipleServices() throws CompanyException {
		Company comp = new Company("Test", "Addresse");
		Service serv1 = new Service("Service1");
		Service serv2 = new Service("Service2");
		comp.addService(serv1);
		comp.addService(serv2);
		assertTrue(comp.getServices().size() == 2 && comp.getServices().contains(serv1)
				&& comp.getServices().contains(serv2));
	}

	@Test
	public void addSameService() throws CompanyException {
		Company comp = new Company("Test", "Addresse");
		Service serv = new Service("Service");
		comp.addService(serv);
		assertThrows(CompanyException.class, () -> comp.addService(serv));
	}
	// =======================

	// == Remove service tests ==
	@Test
	public void removeService() throws CompanyException {
		Company comp = new Company("Test", "Addresse");
		Service serv = new Service("Service");
		comp.addService(serv);
		comp.removeService(serv);
		assertTrue(comp.getServices().isEmpty() && !comp.getServices().contains(serv));
	}
	// ==========================
}
