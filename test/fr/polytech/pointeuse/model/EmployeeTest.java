package fr.polytech.pointeuse.model;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import fr.polytech.pointeuse.model.company.Employee;
import fr.polytech.pointeuse.model.company.Service;

public class EmployeeTest {

	// == Change service tests ==
	@Test
	public void changeService() {
		Employee emp = new Employee("A", "B");
		Service serv = new Service("Service");
		emp.changeService(serv);
		assertTrue(emp.getService().equals(serv));
	}
	// ==========================
}
