package fr.polytech.pointeuse.dao;

import fr.polytech.pointeuse.model.company.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Disabled
class EmployeeDAOTest {
    EmployeeDAO employeeDAO;

    @BeforeEach
    void before(){
        employeeDAO = new EmployeeDAO();
    }


    @Test
    void getAllEmployees() {
        List<Employee> employees = employeeDAO.getAllEmployees();
        assertNotNull(employees);
        assertFalse(employees.isEmpty());
    }


    @Test
    void getEmployeeById(){
        Employee employee = employeeDAO.getEmployeeById(01);
        assertNotNull(employee);
        assertEquals(01, employee.getId());
    }

    @Test
    void saveEmployee(){
       fail();
    }
}